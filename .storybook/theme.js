import { create } from '@storybook/theming';

import logo from './public/logo.svg';

export default create({
  // Brand info
  base: 'dark',
  color: '#fff',
  textColor: '#fff',
  brandTitle: 'Shakuro Custom Theme',
  brandUrl: 'https://shakuro.com',
  brandImage: logo,
  // Main
  appBg: '#6B15D4',
  appContentBg: '#2c2c3a',
  appBorderColor: '#545469',
  // Bar
  barBg: '#2c2c3a',
  barTextColor: '#fff',
  // Inputs
  inputBg: 'transparent',
  inputBorder: '#545469',
})
