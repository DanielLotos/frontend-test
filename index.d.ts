declare module '*.jpg';
declare module '*.jpeg';
declare module '*.png';
declare module '*.svg';
declare module '*.ico';
declare module '*.woff';
declare module '*.woff2';
declare module '*.mp4';

declare module 'browser-image-size' {
  declare function browserImageSize(
    image: string | File | Blob,
  ): Promise<{ width?: number; height?: number }>;
  export default browserImageSize;
}

/**
 * for custom primitive types for example:
 *
 * // date string in HH:MM format
 * type TimeString = string;
 * type Test = { time: TimeString; };
 * const t: Test = { time: '10:20' };
 * // (property) time: string - here we loss 'TimeString'
 * t.s;
 *
 * -------------------
 * OR we can use Opaque type
 * -------------------
 *
 * // date string in HH:MM format
 * type TimeString = Opaque<string, 'TimeString'>;
 * type Test = { time: TimeString; };
 * const t: Test = { time: '10:20' };
 * // (property) time: TimeString - now we can jump into TimeString definition and se info about format
 * t.s;
 */
// eslint-disable-next-line @typescript-eslint/naming-convention
type Opaque<T, K> = T & { __opaque__: K };

type Prettify<T> = {
  [K in keyof T]: T[K];
};

type DeepPartial<T> = {
  [P in keyof T]?: DeepPartial<T[P]>;
};

type GetComponentProps<T> = React.ComponentProps<T>;

/**
transforms:
const arr = [...] as const;
const length = arr.length // 3

type Test = IndexesOfArray<typeof slots> // '0' | '1' | '2'
 */
type IndexesOfArray<A> = Exclude<keyof A, keyof []>;

/** Global API types */
type BackendEntity = 'user' | 'category' | 'post' | 'modality' | 'event' | 'profile';

type ID = string | number;

/** date string like: 2016-05-17 */
type DateString = string;

/** UTC date time string like: 2019-04-27T12:39:39Z */
type DateTimeString = string;

/** price in cents */
type Money = number;

interface APIResponse<T, M = Record<string, unknown>> {
  payload: T;
  meta?: M;
}

interface APIListResponse<T, M = Record<string, unknown>> {
  payload: T[];
  meta: {
    page: number;
    size: number;
    totalPages: number;
    totalCount: number;
  } & M;
}

interface APIError<T = unknown> {
  error: string;
  errors: Record<string, any>;
  payload: T;
  statusCode: number;
}

type APIListParams = {
  page?: number;
  size?: number;
};
/** END Global API types */

type EqualsTest<T> = <A>() => A extends T ? 1 : 0;

type Equals<A1, A2> = EqualsTest<A2> extends EqualsTest<A1> ? 1 : 0;

type Filter<K, I> = Equals<K, I> extends 1 ? never : K;

/**
 * Allows to remove index signature definition from given type
 * @typedef OmitIndex
 * @example
 *
 * type Foo = {
 *  foo: boolean;
 *  bar: string;
 *  [key: string]: any;
 * }
 *
 * type FooKeys = keyof Foo; // string | number
 *
 * type CleanFoo = OmitIndex<Foo>;
 *
 * type CleanFooKeys = keyof CleanFoo; // "foo" | "bar"
 */
type OmitIndex<T, I extends string | number = string | number> = I extends unknown
  ? {
      [K in keyof T as Filter<K, I>]: T[K];
    }
  : never;

/**
 * custom types for composable components
 */
type WithChildrenIfReactComponentClass<C extends string | React.ComponentType<any>> =
  // eslint-disable-next-line @typescript-eslint/ban-types
  C extends React.ComponentClass<any> ? { children?: React.ReactNode } : {};

type ComposableComponentDefaultProps = {
  children?: React.ReactNode;
  className?: string;
  style?: React.CSSProperties;
};

type ComposableComponentProps<
  C extends string | React.ComponentType<any>,
  // eslint-disable-next-line @typescript-eslint/ban-types
  P extends object,
  // eslint-disable-next-line @typescript-eslint/ban-types
> = P extends object
  ? P &
      ComposableComponentDefaultProps &
      Omit<React.ComponentPropsWithRef<C extends React.ComponentType<any> ? C : any>, never> &
      WithChildrenIfReactComponentClass<C>
  : never;

type ComposableComponentPropsWithAs<
  C extends string | React.ComponentType<any>,
  // eslint-disable-next-line @typescript-eslint/ban-types
  P extends object,
> = ComposableComponentProps<C, P> & { as?: C };

type ForwardRefExoticBase<P> = Pick<
  React.ForwardRefExoticComponent<P>,
  keyof React.ForwardRefExoticComponent<any>
>;

interface ComposableComponent<
  C extends string | React.ComponentType<any> = string,
  // eslint-disable-next-line @typescript-eslint/ban-types
  P extends object = {},
> extends ForwardRefExoticBase<ComposableComponentPropsWithAs<C, P>> {
  <As extends string | React.ComponentType<any> = C>(
    props: ComposableComponentPropsWithAs<As, P>,
  ): React.ReactElement<ComposableComponentPropsWithAs<As, P>>;
}

type Prettify<T> = {
  [K in keyof T]: T[K];
};
