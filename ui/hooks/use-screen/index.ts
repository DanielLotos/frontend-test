import { useMedia } from 'react-use';

import { screens } from '../../theme/screens';

export type Screen = keyof typeof screens;

export const useScreen = (screen: Screen, defaultValue = true) => {
  return useMedia(`(min-width: ${screens[screen]})`, defaultValue);
};
