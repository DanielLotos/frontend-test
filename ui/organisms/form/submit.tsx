import { useFormState } from 'react-hook-form';

import { createComponent } from '@sh/ui/create-component';

export const Submit = createComponent(({ as: Component = 'button', ...rest }, ref) => {
  const { isSubmitting } = useFormState();

  return (
    <Component ref={ref} disabled={isSubmitting} loading={isSubmitting} type="submit" {...rest} />
  );
});
