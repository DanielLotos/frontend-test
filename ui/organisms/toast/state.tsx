import { cloneElement, ReactNode } from 'react';
import { nanoid } from 'nanoid';
import { proxy, ref } from 'valtio';

import * as Toast from './toast';

export const state = proxy<{ toasts: { id: string; content: JSX.Element }[] }>({ toasts: [] });

export const removeToast = (id: string) => {
  state.toasts = state.toasts.filter(toast => {
    return toast.id !== id;
  });
};

export const toast = (content: JSX.Element, customToastId?: string) => {
  if (content.type !== Toast.Root) {
    throw new Error('`toast` should be called with `Toast.Root` component instance as argument');
  }

  const id = customToastId || nanoid();

  state.toasts.push({
    id,
    content: ref(
      cloneElement(content, {
        onOpenChange: (isOpen: boolean) => {
          if (isOpen) return;
          removeToast(id);
        },
      }),
    ),
  });

  return id;
};

const defaultToast =
  (variant: Toast.ToastVariants) =>
  (description: ReactNode, action?: JSX.Element, customToastId?: string) =>
    toast(
      <Toast.Root className="flex items-center justify-between" variant={variant}>
        <div className="flex items-center gap-1">
          <Toast.Icon variant={variant} />
          <Toast.Description>{description}</Toast.Description>
        </div>
        <div className="flex items-center gap-1">
          {action}
          <Toast.Close />
        </div>
      </Toast.Root>,
      customToastId,
    );

toast.success = defaultToast('success');

toast.error = defaultToast('error');

toast.warning = defaultToast('warning');

toast.info = defaultToast('info');
