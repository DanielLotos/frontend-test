import { FC, Fragment, PropsWithChildren } from 'react';
import { Provider, ToastProviderProps, Viewport } from '@radix-ui/react-toast';
import { useSnapshot } from 'valtio';

import { state } from './state';

export const ToastProvider: FC<PropsWithChildren<ToastProviderProps>> = ({
  children,
  ...props
}) => {
  const { toasts } = useSnapshot(state);

  return (
    <Provider {...props}>
      {children}
      {toasts.map(toast => (
        <Fragment key={toast.id}>{toast.content}</Fragment>
      ))}
      <Viewport className="fixed right-0 top-vh-vvh-diff z-toast grid max-h-viewport w-full gap-2 p-2 sm:bottom-auto sm:top-0 sm:w-[390px]" />
    </Provider>
  );
};
