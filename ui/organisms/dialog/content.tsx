import { FC, PropsWithChildren, useCallback, useMemo } from 'react';
import { Modal } from '@mui/base/Modal';
import clsx from 'clsx';

import { stopPropagation } from '@sh/core';

import { Transition } from '../../atoms';
import { DialogBackdrop, DialogBackdropProps } from './backdrop';
import { useDialog } from './use-dialog';

export type DialogContentProps = PropsWithChildren<{
  className?: string;
  /**
   * If true, the modal will not automatically shift focus to itself when it opens,
   * and replace it to the last focused element when it closes.
   */
  disableAutoFocus?: boolean;
  /**
   * If `true`, hitting backdrop will not fire the onClose callback.
   */
  disableBackdropClick?: boolean;
  /**
   * If `true`, hitting escape will not fire the onClose callback.
   */
  disableEscapeKeyDown?: boolean;
  /**
   * The `children` will be under the DOM hierarchy of the parent component.
   */
  disablePortal?: boolean;
  disableScrollLock?: boolean;
  /**
   * If `true`, removes content margins, it makes possible make Dialog fullscreen
   */
  fullScreen?: boolean;
  variant?: 'primary' | 'drawer' | 'custom';
  backdropProps?: DialogBackdropProps;
  transitionClassName?: string;
  rootClassName?: string;
}>;

const slots = {
  backdrop: DialogBackdrop,
} as const;

export const DialogContent: FC<DialogContentProps> = ({
  backdropProps,
  children,
  className,
  disableBackdropClick,
  fullScreen,
  rootClassName,
  transitionClassName,
  variant = 'primary',
  ...props
}) => {
  const { titleId, isOpen, descriptionId, actions } = useDialog();

  const handleBackdropClick = useCallback(() => {
    if (disableBackdropClick) return;

    actions.close();
  }, [disableBackdropClick, actions]);

  const slotProps = useMemo(
    () => ({
      root: {
        className: clsx('fixed left-0 top-0 z-dialog h-full w-full outline-0', rootClassName),
      },
      backdrop: backdropProps,
    }),
    [backdropProps, rootClassName],
  );

  return (
    <>
      <Modal
        {...props}
        aria-describedby={descriptionId}
        aria-labelledby={titleId}
        closeAfterTransition
        open={isOpen}
        slotProps={slotProps}
        slots={slots}
        onClose={actions.close}
      >
        <Transition
          ref={actions.getContentRef}
          className={clsx(
            'absolute flex h-full w-full overflow-auto outline-0',
            !transitionClassName &&
              variant !== 'drawer' &&
              'motion-safe:d-state-closed:animate-zoom-out motion-safe:d-state-open:animate-zoom-in',
            !transitionClassName &&
              variant === 'drawer' &&
              'motion-safe:d-state-closed:animate-slide-out-left motion-safe:d-state-open:animate-slide-in-left',
            transitionClassName,
          )}
          in={isOpen}
          role="presentation"
          onClick={handleBackdropClick}
        >
          <div
            className={clsx(
              variant !== 'drawer' && 'm-auto flex w-full items-center justify-center',
            )}
          >
            <div
              className={clsx(
                !fullScreen && 'm-2 sm:m-3 lg:m-6',
                variant === 'primary' && 'max-w-modal bg-bg200 shadow-shadow',
                className,
              )}
              role="presentation"
              onClick={stopPropagation}
            >
              {children}
            </div>
          </div>
        </Transition>
      </Modal>
    </>
  );
};
