import { forwardRef } from 'react';
import clsx from 'clsx';

export type DialogBackdropProps = {
  className?: string;
  disabled?: boolean;
  open?: boolean;
  variant?: 'dark' | 'light';
  // material internals should never be used
  ownerState?: unknown;
};

export const DialogBackdrop = forwardRef<HTMLDivElement, DialogBackdropProps>(
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  ({ className, open, ownerState, variant = 'dark', disabled, ...rest }, ref) => {
    if (disabled) return null;

    return (
      <div
        {...rest}
        ref={ref}
        aria-hidden="true"
        className={clsx(
          'fixed left-0 top-0 min-h-full w-full',
          variant === 'dark' && 'bg-bg685',
          variant === 'light' && 'bg-bg700/[.85]',
          open
            ? 'animate-[fadeIn_0.2s_ease-in-out]'
            : 'animate-[fadeOut_0.2s_ease-in-out_forwards]',
          className,
        )}
      />
    );
  },
);
