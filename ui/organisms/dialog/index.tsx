import { FC } from 'react';
import dynamic from 'next/dynamic';

import { Spinner } from '../../atoms';
import { createComponent } from '../../create-component';
import type { DialogContentProps } from './content';
import { useDialog, useDialogActions } from './use-dialog';

export const Trigger = createComponent(({ as: Component = 'button', ...rest }, ref) => {
  const { open } = useDialogActions();

  return <Component ref={ref} {...rest} onClick={open} />;
});

export const Close = createComponent(({ as: Component = 'button', ...rest }, ref) => {
  const { close } = useDialogActions();

  return <Component ref={ref} {...rest} onClick={close} />;
});

export const Title = createComponent(({ as: Component = 'h1', ...rest }, ref) => {
  const { titleId } = useDialog();

  return <Component ref={ref} id={titleId} {...rest} />;
});

export const Description = createComponent(({ as: Component = 'p', ...rest }, ref) => {
  const { descriptionId } = useDialog();

  return <Component ref={ref} id={descriptionId} {...rest} />;
});

const ContentComponent = dynamic(
  () =>
    import(/* webpackChunkName: "dialog-content" */ './content').then(mod => ({
      default: mod.DialogContent,
    })),
  {
    loading: () => <Spinner />,
  },
);

export const Content: FC<DialogContentProps> = props => <ContentComponent {...props} />;

export { DialogProvider as Root } from './use-dialog';
