import { useMemo } from 'react';
import clsx from 'clsx';

import { createComponent } from '../../create-component';

type CircularProgressProps = {
  progress?: number;
  size?: number;
  strokeWidth?: number;
};

const STROKE_SEGMENTS = 112;

export const CircularProgress = createComponent<CircularProgressProps>(
  ({ strokeWidth = 4, size = 40, progress = 0 }, ref) => {
    const circleStyles = useMemo(
      () => ({
        strokeLinecap: 'round',
        strokeWidth,
        transform: 'rotate(-90deg)',
        strokeDasharray: STROKE_SEGMENTS,
        transformOrigin: '50% 50%',
        strokeDashoffset: STROKE_SEGMENTS - STROKE_SEGMENTS * (progress / 100.0),
        ['--progress-start']: STROKE_SEGMENTS,
        ['--progress-offset']: STROKE_SEGMENTS - STROKE_SEGMENTS * (progress / 100.0),
      }),
      [progress, strokeWidth],
    );

    return (
      <svg ref={ref} style={{ width: size, height: size }} viewBox="0 0 40 40">
        <circle
          className="fill-transparent stroke-[rgba(222,232,241,0.4)] stroke-[4]"
          cx="20"
          cy="20"
          r={20 - strokeWidth / 2}
          style={{
            strokeWidth,
          }}
        />
        <circle
          className={clsx(
            'fill-transparent stroke-primary100 stroke-[4] transition-[stroke-dashoffset]',
          )}
          cx="20"
          cy="20"
          r={20 - strokeWidth / 2}
          style={{ ...circleStyles } as any}
        />
      </svg>
    );
  },
);
