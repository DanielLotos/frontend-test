import { createComponent } from '../../create-component';

type LinearProgressProps = {
  progress?: number;
  width?: number;
  height?: number;
};

export const LinearProgress = createComponent<LinearProgressProps>(
  ({ width = 180, height = 4, progress = 0 }, ref) => {
    return (
      <div ref={ref} className="bg-primary300" style={{ width, height }}>
        <div className="bg-primary100" style={{ width: (width * progress) / 100.0, height }} />
      </div>
    );
  },
);
