import { FC } from 'react';
import clsx from 'clsx';

import primary from './primary.svg';
import primaryDark from './primary-dark.svg';
import primaryShort from './primary-short.svg';

export type LogoProps = Prettify<
  React.HTMLAttributes<HTMLImageElement> & {
    variant?: 'primary' | 'primary-short' | 'primary-dark';
    height?: number;
    width?: number;
  }
>;

const logoProps = {
  primary: {
    height: 36,
    src: primary,
    width: 124,
  },

  'primary-dark': {
    height: 36,
    src: primaryDark,
    width: 124,
  },

  'primary-short': {
    height: 72,
    src: primaryShort,
    width: 72,
  },
};

export const Logo: FC<LogoProps> = ({ variant = 'primary', className, ...rest }) => {
  const extra = logoProps[variant];

  return (
    // eslint-disable-next-line @next/next/no-img-element
    <img alt="TestTask logo" className={clsx('shrink-0', className)} {...extra} {...rest} />
  );
};
