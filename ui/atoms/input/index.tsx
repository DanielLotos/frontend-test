import { ChangeEvent, InputHTMLAttributes } from 'react';
import clsx from 'clsx';

import { createComponent } from '../../create-component';

type InputVariant = 'primary' | 'secondary' | 'search' | 'schedule';

type InputSize = '200' | '100';

export type InputProps = {
  icon?: boolean;
  as?: string;
  autoComplete?: string;
  autoFocus?: boolean;
  className?: string;
  defaultValue?: string;
  invalid?: boolean;
  maxLength?: string;
  name?: string;
  onChange?: (value: string | ChangeEvent<HTMLInputElement>) => void;
  placeholder?: string;
  rows?: number;
  variant?: InputVariant;
  size?: InputSize;
  type?: HTMLInputElement['type'];
  value?: string;
  enterKeyHint?: InputHTMLAttributes<HTMLInputElement>['enterKeyHint'];
};

const getVariantClassNames = (variant: InputVariant, invalid: boolean | undefined) => {
  const variants: Record<InputVariant, any> = {
    primary: clsx(
      'rounded-full border border-outline500 bg-bg100 text-txt900',
      'hover:border-outline500',
      'focus:border-outline900',
      'focus-visible:ring-0 disabled:border-bg-disabled300 disabled:bg-bg-disabled200 disabled:text-bg-disabled300',
      invalid && '!border-error600 focus:!border-error600',
    ),
    secondary: clsx(
      'rounded-full border border-outline500 bg-bg100 text-txt900',
      'hover:border-outline500',
      'focus:border-outline900',
      'focus-visible:ring-0 disabled:border-bg-disabled300 disabled:bg-bg-disabled200 disabled:text-bg-disabled300',
      invalid && '!border-error600 focus:!border-error600',
    ),
    search: clsx(
      'border-b border-txt600 bg-transparent text-txt900',
      'focus:border-primary100',
      'focus-visible:ring-0 disabled:border-bg-disabled300 disabled:bg-bg-disabled200 disabled:text-bg-disabled300',
      invalid && 'border-error600 focus:border-error600',
    ),
    schedule: clsx(
      '!v-btn200 rounded-full border border-bg100 bg-bg100 text-primary100',
      'hover:border-outline500',
      'focus:border-outline900',
      'focus-visible:ring-0 disabled:border-bg-disabled300 disabled:bg-bg-disabled200 disabled:text-bg-disabled300',
      invalid && '!border-error600 focus:!border-error600',
    ),
  };

  return variants[variant];
};

const getSizesClassNames = (size: InputSize) => {
  const sizes: Record<InputSize, any> = {
    200: clsx('v-input-val100 px-3 py-2 [&[type=date]]:min-h-5 [&[type=time]]:min-h-5'),
    100: clsx('v-input-val100 px-3 py-1 [&[type=date]]:min-h-5 [&[type=time]]:min-h-5'),
  };

  return sizes[size];
};

export const Input = createComponent<InputProps>(
  (
    {
      className,
      size = '200',
      invalid,
      as: Component = 'input',
      variant = 'primary' as const,
      icon,
      ...rest
    },
    ref,
  ) => {
    return (
      <Component
        ref={ref}
        autoCorrect="off"
        className={clsx(
          'block w-full appearance-none transition-colors duration-300 [textarea&]:resize-none',
          getVariantClassNames(variant, invalid),
          getSizesClassNames(size),
          icon && 'pl-5',
          className,
        )}
        {...rest}
      />
    );
  },
);
