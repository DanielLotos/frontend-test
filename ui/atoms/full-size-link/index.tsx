import clsx from 'clsx';

import { createComponent } from '../../create-component';

export const FullSizeLink = createComponent(({ as: Component = 'a', className, ...rest }, ref) => {
  return (
    <Component
      ref={ref}
      className={clsx(
        "before:absolute before:bottom-0 before:left-0 before:right-0 before:top-0 before:content-['']",
        'rounded-[inherit] before:rounded-[inherit]',
        'focus-visible:ring-0 before:focus-visible:ring-2 before:focus-visible:ring-focus100',
        className,
      )}
      {...rest}
    />
  );
});
