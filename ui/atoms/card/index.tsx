import clsx from 'clsx';

import { createComponent } from '../../create-component';

export const Card = createComponent(({ as: Component = 'div', className, ...rest }, ref) => {
  return (
    <Component
      ref={ref}
      className={clsx('p-2 sm:p-3 md:px-4 md:py-3', 'bg-bg100 shadow-card-shadow', className)}
      {...rest}
    />
  );
});
