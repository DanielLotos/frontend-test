import { cloneElement, forwardRef, ReactNode } from 'react';
import { useIsomorphicLayoutEffect } from 'react-use';
import { useComposedRefs } from '@radix-ui/react-compose-refs';
import { Presence } from '@radix-ui/react-presence';
import clsx from 'clsx';

import { createComponent } from '../../create-component';

export type TransitionProps = {
  children: ReactNode;
  in: boolean;
  onEnter?: () => void;
  onExited?: () => void;
};

const ChildWithCallbacks = forwardRef<any, Omit<TransitionProps, 'in'> & { children: ReactNode }>(
  ({ onEnter, onExited, children }, ref) => {
    const composedRef = useComposedRefs(ref, (children as any).ref);

    useIsomorphicLayoutEffect(() => {
      onEnter?.();

      return () => onExited?.();
    }, []);

    return cloneElement(children as any, { ref: composedRef });
  },
);

export const Transition = createComponent<TransitionProps>(
  ({ className, onEnter, onExited, as: Component = 'div', in: present = false, ...rest }, ref) => {
    return (
      <Presence present={present}>
        <ChildWithCallbacks onEnter={onEnter} onExited={onExited}>
          <Component
            ref={ref}
            className={clsx('', className)}
            data-state={present ? 'open' : 'closed'}
            {...rest}
          />
        </ChildWithCallbacks>
      </Presence>
    );
  },
);
