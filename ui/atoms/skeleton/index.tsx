import { FC, ReactNode, useMemo } from 'react';
import clsx from 'clsx';

export type SkeletonProps = {
  count?: number;
  className?: string;
  circle?: boolean;
};

export const Skeleton: FC<SkeletonProps> = ({
  count = 1,
  circle = false,
  className = '',
  ...rest
}) => {
  const elements = useMemo(() => new Array(count).fill(1), [count]);

  return (
    <>
      {elements.map((_, index) => (
        <span
          key={index}
          {...rest}
          className={clsx(
            'inline-flex w-full animate-pulse align-middle leading-none',
            'bg-outline300',
            circle ? 'rounded-full' : 'rounded-sm',
            className,
          )}
        >
          &nbsp;
        </span>
      ))}
    </>
  );
};

export const SkeletonOrChildren = ({
  children,
  showSkeleton,
  ...props
}: {
  children: ReactNode;
  showSkeleton?: boolean;
} & SkeletonProps) => {
  if (showSkeleton) return <Skeleton {...props} />;

  return children;
};
