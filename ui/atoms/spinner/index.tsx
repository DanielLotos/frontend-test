import { FC, SVGProps } from 'react';
import { useTimeout } from 'react-use';

export type SpinnerProps = SVGProps<SVGSVGElement> & {
  /** @default 300 */
  showDelay?: number;
  size?: number;
  strokeWidth?: number;
};

export const Spinner: FC<SpinnerProps> = ({
  showDelay = 300,
  size = 24,
  strokeWidth = 2,
  ...rest
}) => {
  const center = size / 2;
  const maxSize = center - strokeWidth;
  const [isReady] = useTimeout(showDelay);

  if (!isReady()) return null;

  return (
    <svg style={{ width: size, height: size, stroke: 'currentColor' }} {...rest}>
      <g fill="none" fillRule="evenodd" strokeWidth={strokeWidth}>
        <circle cx={center} cy={center} r={1}>
          <animate
            attributeName="r"
            begin="0s"
            calcMode="spline"
            dur="1.8s"
            keySplines="0.165, 0.84, 0.44, 1"
            keyTimes="0; 1"
            repeatCount="indefinite"
            values={`1; ${maxSize}`}
          />
          <animate
            attributeName="stroke-opacity"
            begin="0s"
            calcMode="spline"
            dur="1.8s"
            keySplines="0.3, 0.61, 0.355, 1"
            keyTimes="0; 1"
            repeatCount="indefinite"
            values="1; 0"
          />
        </circle>
        <circle cx={center} cy={center} r={1}>
          <animate
            attributeName="r"
            begin="-0.9s"
            calcMode="spline"
            dur="1.8s"
            keySplines="0.165, 0.84, 0.44, 1"
            keyTimes="0; 1"
            repeatCount="indefinite"
            values={`1; ${maxSize}`}
          />
          <animate
            attributeName="stroke-opacity"
            begin="-0.9s"
            calcMode="spline"
            dur="1.8s"
            keySplines="0.3, 0.61, 0.355, 1"
            keyTimes="0; 1"
            repeatCount="indefinite"
            values="1; 0"
          />
        </circle>
      </g>
    </svg>
  );
};
