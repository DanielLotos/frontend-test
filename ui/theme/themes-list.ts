/* eslint-disable max-lines */
/* eslint-disable @typescript-eslint/naming-convention */

// THIS FILE IS GENERATED AUTOMATICALLY. DON'T CHANGE IT.

export const DEFAULT_THEME = 'light';

export const THEMES = ['light'] as const;

export type Theme = (typeof THEMES)[number];
