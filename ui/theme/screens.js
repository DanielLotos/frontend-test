const screens = {
  sm: '640px',
  md: '768px',
  lg: '1024px',
  xl: '1280px',
  xxl: '1440px',
};

const container = {
  center: true,
  padding: {
    DEFAULT: '16px',
    sm: '24px',
    md: '32px',
    lg: '48px',
    xl: '52px',
  },
};

module.exports = { screens, container };
