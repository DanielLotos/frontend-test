/* eslint-disable max-lines */
/* eslint-disable @typescript-eslint/naming-convention */
module.exports = {
  boxShadow: {
    'btn-shdw/btn-shdw': '0px 4px 7px rgba(22, 5, 44, 0.29)',
    'hard-shadow/hard-shadow': '11px 21px 68px rgba(0, 0, 0, 0.35)',
    'red-shadow/red-shadow': '0px 4px 7px rgba(233, 42, 42, 0.24)',
    'shadow/shadow': '0px 16px 60px rgba(20, 3, 41, 0.33)',
    'windows-shadow/windows-shadow': '0px 16px 60px rgba(20, 3, 41, 0.13)',
  },
};
