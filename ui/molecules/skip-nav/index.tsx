import { createComponent } from '../../create-component';
import { Button } from '../button';

const defaultId = 'content';

export const Link = createComponent<{ contentId?: string }>(
  ({ as = 'a', contentId = defaultId, children = 'Skip to content', ...rest }, ref) => {
    return (
      <span className="sr-only left-3 top-3 z-dialog focus-within:not-sr-only focus-within:fixed">
        <Button ref={ref} as={as} {...rest} href={`#${contentId}`}>
          {children}
        </Button>
      </span>
    );
  },
);

export const Content = createComponent<{ contentId?: string }>(
  ({ as: Component = 'div', contentId = defaultId, ...rest }, ref) => {
    return <Component ref={ref} {...rest} id={contentId} />;
  },
);
