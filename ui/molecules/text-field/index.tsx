import { FC, forwardRef, PropsWithChildren, ReactNode } from 'react';
import clsx from 'clsx';

import { FieldError, Icon, Input, InputProps } from '../../atoms';
import { IconsType } from '../../atoms/icon/types';

type InputSize = '200' | '100';

export type TextFieldProps = Prettify<
  {
    size?: InputSize;
    error?: string;
    label?: ReactNode;
    inputClassName?: string;
    iconName?: string;
    children?: ReactNode;
  } & InputProps
>;

export const TextField: FC<PropsWithChildren<TextFieldProps>> = forwardRef(
  (
    {
      children,
      className,
      inputClassName,
      error,
      label,
      name,
      onChange,
      size = '200',
      value = '',
      iconName,
      ...rest
    },
    ref,
  ) => {
    return (
      <label className={clsx('inline-flex appearance-none flex-col', className)}>
        {label && <div className="v-input-label200">{label}</div>}
        <div className="relative flex flex-col gap-4px">
          {iconName && (
            <span className="absolute left-1 top-1/2 z-10 flex -translate-y-1/2 outline-none transition">
              <Icon name={iconName as IconsType} size={size === '200' ? 24 : 20} />
            </span>
          )}
          <Input
            ref={ref}
            aria-invalid={!!error}
            autoCorrect="off"
            className={clsx(inputClassName)}
            {...{ name, value }}
            icon={!!iconName}
            invalid={!!error}
            size={size as any}
            onChange={onChange as any}
            {...rest}
          />
          {children}
          {error && <FieldError>{error}</FieldError>}
        </div>
      </label>
    );
  },
);
