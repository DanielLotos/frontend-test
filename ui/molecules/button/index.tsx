import { cva, VariantProps } from 'cva';

import { Spinner } from '../../atoms/spinner';
import { createComponent } from '../../create-component';

export type ButtonStyles = VariantProps<typeof buttonStyles>;

export type ButtonProps = {
  disabled?: boolean;
  loading?: boolean;
  type?: 'submit' | 'button';
} & ButtonStyles;

export const Button = createComponent<ButtonProps>(
  (
    {
      as: Component = 'button',
      children,
      className,
      disabled,
      iconOnly,
      loading,
      size = '200',
      type,
      variant = 'primary',
      ...rest
    },
    ref,
  ) => {
    const resultType = Component === 'button' && !type ? 'button' : type;
    const resultDisabled = disabled || loading ? true : disabled;

    return (
      <Component
        ref={ref}
        className={buttonStyles({ variant, size, iconOnly, loading, className })}
        disabled={resultDisabled}
        // eslint-disable-next-line react/button-has-type
        type={resultType}
        {...rest}
      >
        {children}
        {loading && (
          <Spinner className={spinnerStyles({ variant })} size={size === '200' ? 24 : 16} />
        )}
      </Component>
    );
  },
);

const buttonStyles = cva({
  base: [
    'inline-flex appearance-none items-center justify-center gap-4px transition delay-150',
    'v-btn200 relative',
    'cursor-pointer focus-visible:ring-4 focus-visible:ring-focus100 disabled:cursor-auto',
  ],
  variants: {
    variant: {
      primary: [
        'rounded-full bg-primary100 text-txt100',
        'hover:bg-hover900 focus:bg-pressed900',
        'disabled:bg-bg-disabled300 disabled:text-content-disabled100 disabled:shadow-transparent',
        'shadow-btn-shdw focus:shadow-[0_3px_3px_0_#0000001f_inset]',
      ],
      'filled-white': [
        'rounded-full',
        'bg-bg100 text-primary100 hover:bg-bg-hover50 focus:bg-bg-pressed100',
        'disabled:bg-bg-disabled100 disabled:text-content-disabled200 disabled:shadow-transparent',
      ],
      alert: [
        'rounded-full bg-error600 text-txt100',
        'hover:bg-hover600 focus:bg-pressed600',
        'disabled:bg-bg-disabled300 disabled:text-content-disabled100 disabled:shadow-transparent',
        'shadow-red-shadow',
      ],
      filled: [
        'rounded-full bg-primary150 text-primary100',
        'hover:bg-hover820 focus:bg-pressed820',
        'disabled:bg-bg-disabled200 disabled:text-content-disabled200',
      ],
      'clear-violet': [
        'rounded-full',
        'text-primary100 hover:text-hover900 focus:text-pressed900',
        'disabled:text-content-disabled200',
      ],
      clear: [
        'rounded-full',
        'text-scn900 hover:text-hover900 focus:text-pressed900',
        'disabled:text-content-disabled200',
      ],
      outline: [
        'rounded-full border border-primary200',
        'text-primary100 hover:border-hover820 hover:text-hover900 focus:text-pressed900',
        'disabled:border-bg-disabled200 disabled:text-content-disabled200',
      ],
      'filled-white-secondary': [
        'rounded-full',
        'bg-bg12 text-txt100 hover:text-hover100 focus:text-txt100',
        'disabled:text-content-disabled100',
      ],
    },
    size: {
      '300': ['v-btn200 min-h-[54px] px-3 py-2'],
      '200': ['v-btn100 min-h-[44px] px-2'],
      '100': ['v-btn50 h-4 px-12px py-1'],
    },
    iconOnly: {
      true: '!p-0',
    },
    loading: {
      true: '!text-transparent',
    },
  },
  compoundVariants: [
    {
      iconOnly: true,
      size: '300',
      className: '!p-2',
    },
    {
      iconOnly: true,
      size: '200',
      className: '!min-w-[44px]',
    },
    {
      iconOnly: true,
      size: '100',
      className: '!p-4px',
    },

    {
      variant: 'primary',
      loading: true,
      className: '!bg-primary100',
    },
    {
      variant: 'filled-white',
      loading: true,
      className: '!bg-bg100 !shadow-btn-shdw',
    },
    {
      variant: 'alert',
      loading: true,
      className: '!bg-error600',
    },
    {
      variant: 'filled',
      loading: true,
      className: '!bg-primary150',
    },

    {
      variant: 'filled-white',
      size: '300',
      className: 'shadow-btn-shdw focus:shadow-[0_3px_3px_0_#0000001f_inset]',
    },
  ],
});

const spinnerStyles = cva({
  base: 'absolute left-1/2 -translate-x-1/2 transform self-center',
  variants: {
    variant: {
      primary: ['text-txt100'],
      'filled-white': ['text-primary100'],
      alert: ['text-txt100'],
      filled: ['text-primary100'],
      'clear-violet': ['text-primary100'],
      clear: ['text-scn900'],
      outline: '',
      'filled-white-secondary': '',
    },
  },
});
