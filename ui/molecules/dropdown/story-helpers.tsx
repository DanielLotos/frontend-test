import { FC } from 'react';
import { DropdownMenuSubTriggerProps } from '@radix-ui/react-dropdown-menu';

import {
  Content as DropdownContent,
  DropdownContentProps,
  DropdownItemProps,
  Item as DropdownItem,
  Trigger,
} from '.';

export const Content = DropdownContent as FC<DropdownContentProps>;
export const Item = DropdownItem as FC<DropdownItemProps>;
export const TriggerItem = Trigger as FC<DropdownMenuSubTriggerProps>;

export { Label, Root, Separator, Trigger } from '.';
