import { FC, forwardRef } from 'react';
import clsx from 'clsx';

import { Icon } from '@sh/ui/atoms';

import { TextField, TextFieldProps } from '../text-field';

export const SearchField: FC<TextFieldProps & { ref?: any }> = forwardRef(
  ({ type: _, inputClassName, ...props }, ref) => {
    return (
      <TextField ref={ref} {...props} inputClassName={clsx('pr-4', inputClassName)}>
        <span className="absolute left-0 top-1/2 z-10 flex -translate-y-1/2 outline-none transition hover:opacity-70 focus:text-primary100 focus:opacity-100">
          <Icon name="search" size={props.size === '200' ? 20 : 20} />
        </span>
      </TextField>
    );
  },
);
