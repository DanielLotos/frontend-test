import { FC, ReactNode, useMemo } from 'react';
import clsx from 'clsx';

import { Icon } from '../../atoms';
import { createComponent } from '../../create-component';
import { Button } from '../button';
import { CarouselDragFreeProvider, useCarouselDragFree } from './provider';
import { useNavButtons } from './use-nav-buttons';

export type CarouselDragFreeProps = {
  totalSlides: number;
  renderSlide: FC<{ index: number }>;
  title?: ReactNode;
  className?: string;
  containerClassName?: string;
};

export const Root = CarouselDragFreeProvider;

export const Wrapper = createComponent(({ as: Component = 'div', className, ...rest }, ref) => {
  return <Component ref={ref} className={clsx('overflow-hidden', className)} {...rest} />;
});

export const Content: FC<CarouselDragFreeProps> = ({
  renderSlide: Component,
  totalSlides,
  className,
  containerClassName,
}) => {
  const slides = useMemo(() => new Array(totalSlides).fill(777), [totalSlides]);
  const [emblaRef] = useCarouselDragFree();

  return (
    <div ref={emblaRef} className={className}>
      <div className={clsx('flex', containerClassName)}>
        {slides.map((_, index) => (
          <Component key={index} index={index} />
        ))}
      </div>
    </div>
  );
};

export const Nav = ({ className }: { className?: string }) => {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [_, emblaApi] = useCarouselDragFree();
  const { prevBtnDisabled, nextBtnDisabled, onPrevButtonClick, onNextButtonClick } =
    useNavButtons(emblaApi);

  return (
    <div className={className}>
      <Button
        aria-label="Previous slide"
        disabled={prevBtnDisabled}
        iconOnly
        size="100"
        variant="clear"
        onClick={onPrevButtonClick}
      >
        <Icon name="link-arrow-left" />
      </Button>
      <Button
        aria-label="Next slide"
        disabled={nextBtnDisabled}
        iconOnly
        size="100"
        variant="clear"
        onClick={onNextButtonClick}
      >
        <Icon name="link-arrow-right" />
      </Button>
    </div>
  );
};
