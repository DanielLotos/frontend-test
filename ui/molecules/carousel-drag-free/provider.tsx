import constate from 'constate';
import { EmblaOptionsType } from 'embla-carousel';
import useEmblaCarousel from 'embla-carousel-react';

export const [CarouselDragFreeProvider, useCarouselDragFree] = constate(
  ({ options }: { options?: EmblaOptionsType }) => {
    return useEmblaCarousel({ dragFree: true, ...options });
  },
);
