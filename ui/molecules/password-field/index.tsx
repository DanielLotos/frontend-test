import { FC, forwardRef } from 'react';
import { useToggle } from 'react-use';

import { Icon } from '@sh/ui/atoms';

import { TextField, TextFieldProps } from '../text-field';

export const PasswordField: FC<TextFieldProps & { withIcon?: boolean; ref?: any }> = forwardRef(
  ({ type: _, withIcon = true, ...props }, ref) => {
    const [showPassword, toggleShowPassword] = useToggle(false);

    return (
      <TextField
        ref={ref}
        type={showPassword ? 'text' : 'password'}
        {...props}
        inputClassName={withIcon ? 'pl-5' : ''}
      >
        {withIcon && (
          <button
            className="absolute left-2 top-1/2 z-10 flex -translate-y-1/2 outline-none transition hover:opacity-70 focus:text-primary100 focus:opacity-100"
            type="button"
            onClick={toggleShowPassword}
          >
            <span className="sr-only">toggle password visibility</span>
            <Icon
              name={showPassword ? 'filled-lock' : 'filled-lock'}
              size={props.size === '200' ? 20 : 16}
            />
          </button>
        )}
      </TextField>
    );
  },
);
