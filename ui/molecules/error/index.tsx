import { FC, ReactNode } from 'react';
import clsx from 'clsx';

import { Icon } from '../../atoms';
import { Button } from '../button';

type ErrorProps = {
  className?: string;
  message?: ReactNode;
  refetch?: () => any;
  variant?: 'dark' | 'light';
};

export const Error: FC<ErrorProps> = ({
  className,
  message = 'Something went wrong. Please try your request again.',
  refetch,
  variant = 'light',
}) => {
  return (
    <div
      className={clsx(
        'flex flex-col items-center justify-between gap-2 rounded-lg p-3 text-center',
        variant === 'dark' && 'bg-bg200',
        variant === 'light' && 'bg-bg100',
        className,
      )}
    >
      <div className="m-auto flex gap-2">
        <Icon className="mb-auto text-warning100" name="warning-circle" size={32} />

        <div className="flex flex-col items-start text-left">
          <p className="v-h400">Error</p>
          <p className="v-p300">{message}</p>
          {refetch && (
            <Button className="mt-1 inline-flex" size="100" variant="filled" onClick={refetch}>
              Retry
            </Button>
          )}
        </div>
      </div>
    </div>
  );
};
