import React, { FC } from 'react';
import { useWindowScroll } from 'react-use';
import clsx from 'clsx';

import { Icon, Transition } from '../../atoms';
import { Button } from '../button';

export const ScrollTop: FC<{ offset?: number }> = ({ offset = 300 }) => {
  const { y } = useWindowScroll();

  const isVisible = y >= offset;

  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth',
    });
  };

  return (
    <Transition
      className={clsx(
        'r-state-closed:animate-fade-out fixed bottom-4 right-4 z-dialog- hidden d-state-open:animate-fade-in lg:flex',
      )}
      in={isVisible ?? false}
    >
      <Button
        className="shadow-shadow"
        iconOnly
        rounded
        size="300"
        variant="filled-white"
        onClick={scrollToTop}
      >
        <Icon className="text-primary100" name="top-arrow" />
      </Button>
    </Transition>
  );
};
