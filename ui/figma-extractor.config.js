// @ts-check
// eslint-disable-next-line @typescript-eslint/no-var-requires
const { screens } = require('./theme/screens');

/**
 * @type {import('@shakuroinc/figma-extractor').Config}
 **/
module.exports = {
  apiKey: 'figd_hdViyW_PeCEsYaPhA-qGVnENWP3pwqMC13hes-rc',
  fileId: 'Yr0knJe52eAu00Pe2AuIfU',
  styles: {
    exportPath: './theme',
    allowedThemes: ['light'], // allowed themes
    defaultTheme: 'light', // one of the allowed themes which will be meant as default theme
    colors: {
      keyName: getKeyNameWithTheme,
      useTheme: true,
    },
    effects: {
      keyName: getKeyNameWithTheme,
      useTheme: true,
    },
    gradients: {},
    textStyles: {
      keyName: nameFromFigma => `.v-${getKeyName(nameFromFigma)}`,
      merge: true,
    },
  },
  icons: [
    // black and white icons
    {
      nodeIds: [
        '5963-15596',
        '5963-15608',
        '7081-27279',
        '6930-85075',
        '7384-35796',
        '6989-51280',
        '6942-88958',
        '7702-55690',
        '7048-29791',
      ],
      iconName: name => name.replace(/ /g, '').replace('/', '-').toLowerCase(),
      exportPath: './atoms/icon',
      generateSprite: false,
      generateTypes: false,
      localIcons: false,
    },
    // For color icons
    // {
    //   nodeIds: [],
    //   iconName: name => name.replace(/ /g, '').replace('/', '-').toLowerCase(),
    //   exportPath: './atoms/icon',
    //   optimizeSvg: false,
    //   generateSprite: false,
    //   generateTypes: false,
    //   localIcons: false,
    // },
    // It's block of the config is to generate sprite and types
    {
      nodeIds: [],
      exportPath: './atoms/icon',
      generateSprite: true,
      generateTypes: true,
      localIcons: true,
    },
  ],
  screens: {
    bs: 0,
    ...screens,
  },
};

/** @param {string} name */
function getKeyName(name = '') {
  if (name.toLowerCase().startsWith('ui-kit') || name.toLowerCase().startsWith('ui kit')) {
    return 'INTERNAL_DO_NOT_USE';
  }

  /**
   * format name from like:
   *  "heading/h800 - md" ->  "h800-md"
   *  "heading / h800 - md" ->  "h800-md"
   *  "conventions are ignored/heading/h800 - md bla bla" -> "h800-md"
   */
  const resultName = name
    .split('/')
    .at(-1)
    ?.replace(' - ', '-')
    .split(' ')
    .find(name => name !== '');

  if (!resultName) {
    throw `getKeyName for "${name}" returns an empty string, check getKeyName implementation`;
  }

  // console.log('getKeyName: ', { name, resultName });

  return resultName;
}

/** @param {string} name */
function getKeyNameWithTheme(name = '') {
  if (name.toLowerCase().startsWith('ui-kit') || name.toLowerCase().startsWith('ui kit')) {
    return 'INTERNAL_DO_NOT_USE';
  }

  const splittedName = name.split('/');

  /**
   * format name from like:
   *  "dark/heading/h800 - md" ->  "h800-md"
   *  "dark / heading / h800 - md" ->  "h800-md"
   */
  let resultName = splittedName
    .at(-1)
    ?.replace(' - ', '-')
    .split(' ')
    .find(name => name !== '');

  resultName = `${splittedName[0]}/${resultName}`;

  if (!resultName) {
    throw `getKeyNameWithTheme for "${name}" returns an empty string, check getKeyName implementation`;
  }

  // eslint-disable-next-line no-console
  console.log('getKeyNameWithTheme: ', { name, resultName });

  return resultName;
}
