# Install dependencies only when needed
FROM node:20-alpine AS deps

# Check https://github.com/nodejs/docker-node/tree/b4117f9333da4138b03a546ec926ef50a31506c3#nodealpine to understand why libc6-compat might be needed.
RUN apk add --no-cache libc6-compat
WORKDIR /opt/node

COPY package.json yarn.lock* .yarnrc ./
COPY frontend/package.json ./frontend/package.json
COPY core/package.json ./core/package.json
COPY ui/package.json ./ui/package.json

RUN yarn policies set-version 1.18.0
RUN yarn install --production --frozen-lockfile && yarn cache clean

# Rebuild the source code only when needed
FROM node:20-alpine AS builder
WORKDIR /opt/node
COPY --from=deps /opt/node/node_modules ./node_modules
COPY --from=deps /opt/node/frontend/node_modules ./frontend/node_modules
COPY --from=deps /opt/node/core/node_modules ./core/node_modules
COPY --from=deps /opt/node/ui/node_modules ./ui/node_modules
COPY . .

ENV NODE_OPTIONS="--max-old-space-size=4096"
ENV NEXT_TELEMETRY_DISABLED 1

# all env vars from jenkins configs will be passed at runtime
# env vars that need be passed at build time should be specified here
# (usually they prefixed with NEXT_PUBLIC_)
ARG NEXT_PUBLIC_API_BASE_URL
ARG NEXT_PUBLIC_APP_URL
ARG NEXT_PUBLIC_IMGPROXY_BUCKET_URL
ARG NEXT_PUBLIC_IMGPROXY_URL
ARG NEXT_PUBLIC_API_ERROR_TRANSFORMER

RUN yarn build

# Production image, copy all the files and run next
FROM node:20-alpine AS runner
RUN apk update && apk add nginx curl
RUN mkdir -p /run/nginx
WORKDIR /opt/node

ENV NODE_ENV production

COPY ./devops/frontend_nginx.conf /etc/nginx/http.d/default.conf
COPY --from=builder /opt/node/frontend/public ./public
COPY --from=builder /opt/node/frontend/package.json ./package.json
COPY --from=builder /opt/node/frontend/.next/standalone ./
COPY --from=builder /opt/node/frontend/.next/static ./.next/static

RUN ln -s /opt/node/.next/ /opt/node/_next

EXPOSE 80

ENV NODE_ENV production
ENV PORT 3001
# set hostname to localhost
ENV HOSTNAME "0.0.0.0"

CMD ["sh", "-c", "nginx; node ./frontend/server.js"]
