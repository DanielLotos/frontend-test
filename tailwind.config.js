/* eslint-disable @typescript-eslint/no-var-requires */
module.exports = {
  // config used only for Storybook and `eslint-plugin-tailwindcss`
  content: ['./frontend/(features|pages)/**/*.{ts,tsx,mdx}', './(core|ui|docs)/**/*.{ts,tsx,mdx}'],
  presets: [require('./ui/tailwind.config')],
};
