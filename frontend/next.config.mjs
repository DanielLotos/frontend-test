// @ts-check
import BundleAnalyzer from '@next/bundle-analyzer';
import CircularDependencyPlugin from 'circular-dependency-plugin';
import CompressionPlugin from 'compression-webpack-plugin';
import { join } from 'node:path';
import { fileURLToPath } from 'node:url';
import { constants } from 'node:zlib';
import { compose } from 'ramda';

import { images } from './next-images.config.mjs';

const withBundleAnalyzer = BundleAnalyzer({
  enabled: process.env.ANALYZE === 'true',
});

const enhance = compose(withBundleAnalyzer);
// eslint-disable-next-line @typescript-eslint/naming-convention
const __dirname = fileURLToPath(new URL('.', import.meta.url));

/**
 * @type {import('next').NextConfig}
 **/
const nextConfig = {
  output: 'standalone',

  eslint: {
    ignoreDuringBuilds: true,
  },

  experimental: {
    // this includes files from the monorepo base
    outputFileTracingRoot: join(__dirname, '../'),
    typedRoutes: true,
  },

  transpilePackages: ['@sh/core', '@sh/ui'],

  i18n: {
    locales: ['en'],
    defaultLocale: 'en',
  },

  images,

  webpack(config, { dev, isServer }) {
    config.plugins.push(
      new CircularDependencyPlugin({
        cwd: process.cwd(),
        exclude: /node_modules/,
        failOnError: true,
      }),
    );

    config.module.rules.push({
      test: /\.svg$/,
      type: 'asset/resource',
      generator: {
        filename: 'static/media/[name]-[hash].[ext]',
      },
    });

    if (!dev && !isServer) {
      config.plugins.push(
        new CompressionPlugin({
          filename: '[path][base].br',
          algorithm: 'brotliCompress',
          test: /\.(js|css|html|svg)$/,
          compressionOptions: {
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            params: {
              [constants.BROTLI_PARAM_QUALITY]: 11,
            },
          },
          threshold: 500,
          minRatio: 0.8,
          deleteOriginalAssets: false,
        }),
      );
    }

    return config;
  },
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  async rewrites() {
    return {
      fallback: process.env.NEXT_PROXY_FALLBACK
        ? [
            {
              source: '/:path*',
              destination: `${process.env.NEXT_PROXY_FALLBACK}/:path*`,
            },
          ]
        : [],
    };
  },
};

export default enhance({
  ...nextConfig,
});
