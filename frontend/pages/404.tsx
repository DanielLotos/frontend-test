import { ErrorPage } from 'features/shared';

const Error404Page = () => <ErrorPage statusCode={404} />;

Error404Page.getLayout = ErrorPage.getLayout;

export default Error404Page;
