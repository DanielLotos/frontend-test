import { ErrorPage as Error } from 'features/shared';

// some times Error from page serialized as string into `ctx.err?.message`
const parseError = (message?: string) => {
  let parsed: any = '';
  try {
    parsed = JSON.parse(message ?? '');
  } catch (e) {}

  return typeof parsed === 'object' ? parsed.statusCode : parsed;
};

/**
 * FIXME: check this later, maybe this behavior already fixed in Next.js (https://github.com/vercel/next.js/issues/39616)
 * for some reasons `statusCode` on client always 500 even if GIP return other code
 * so extract it from `__NEXT_DATA__` on the client
 */
const getStatusCode = (statusCodeFromProps: number) => {
  if (typeof window !== 'object') return statusCodeFromProps;

  // for sometime statusCodeFromProps are valid on client 🤔
  if (statusCodeFromProps !== 500) return statusCodeFromProps;

  try {
    return JSON.parse(document.getElementById('__NEXT_DATA__')?.textContent ?? '').props.pageProps
      .statusCode;
  } catch (e) {
    return statusCodeFromProps;
  }
};

const ErrorPage: NextPage<{ statusCode: number }> = props => {
  const statusCode = getStatusCode(props.statusCode);

  return <Error statusCode={statusCode} />;
};

ErrorPage.getInitialProps = async ctx => {
  const statusCode =
    parseError(ctx.err?.message) || ctx.err?.statusCode || ctx.res?.statusCode || 500;

  if (ctx.res) {
    ctx.res.statusCode = statusCode;
  }

  return { statusCode };
};

ErrorPage.getLayout = Error.getLayout;

export default ErrorPage;
