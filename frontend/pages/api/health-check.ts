import type { NextApiRequest, NextApiResponse } from 'next';

const healthCheck = (req: NextApiRequest, res: NextApiResponse) => {
  res.status(200).json({ message: 'OK' });
};

export default healthCheck;
