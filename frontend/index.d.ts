type NextPage<P = Record<string, unknown>, IP = P> = import('next').NextComponentType<
  import('next').NextPageContext,
  IP,
  P
> & {
  getLayout?(page: import('react').ReactElement): import('react').ReactElement;
};
