/* eslint-disable @typescript-eslint/no-var-requires */
module.exports = {
  content: ['../ui/**/*.{ts,tsx}', './(features|pages)/**/*.{ts,tsx,mdx}'],
  presets: [require('../ui/tailwind.config')],
};
