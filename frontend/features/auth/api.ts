import { QueryClient } from '@tanstack/react-query';

import { apiClient } from '@sh/core';

import { CurrentUserRes, useCurrentUserQuery } from './hooks/use-current-user-query';
import { SignInInput } from './dto';

export const fetchCurrentUser = async (client: QueryClient) => {
  const queryKey = useCurrentUserQuery.getKey(null);
  const user = await client.fetchQuery<CurrentUserRes>({ queryKey }).catch(() => {
    /**
     * important!
     * 1. current user should always resolve
     * 2. if no user - populate cache with null to avoid CSR only for 'publicOnly' pages
     */
    client.setQueryData(queryKey, null);
  });

  return user;
};

export const signInReq = (session: SignInInput) => apiClient.post('/session', { session });

export const signOutReq = () => apiClient.delete('/session');

export const refreshTokenReq = () => apiClient.patch('/refresh_token').then(() => undefined);
