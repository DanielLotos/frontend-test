import { CurrentUserDTO } from './dto';

type AuthParam = {
  emailConfirmed?: boolean;
  /** @default `privateOnly` */
  pageType?: 'publicOnly' | 'privateOnly';
  roles?: CurrentUserDTO['roles'];
  onlyForUser?: boolean;
};

export type AuthParams = AuthParam | AuthParam[];

export enum PassportProvider {
  apple = 'apple',
  facebook = 'facebook',
  google = 'google',
}
