import { UserDTO } from 'features/user';

export type SignInInput = {
  email: string;
  password: string;
  rememberMe?: boolean;
};

export type SignUpInput = {
  user: {
    email: string;
    username?: string;
    firstName: string;
    lastName?: string;
    newsletter: boolean;
    password: string;
    timezone: string;
  };
  socialAccount?: {
    provider: string;
    token: string;
  };
};

export type ForgotPasswordInput = { email: string };

export type SetPasswordViaResetTokenInput = { password: string };

export type ConfirmEmailInput = { token: string };

export type CurrentUserDTO = UserDTO;

/** when we try sign in with social and doesn't have user
 * API return 404 with info to "complete sign up flow"
 * */
export type AuthInfo = {
  id: string;
  firstName?: string;
  lastName?: string;
  email?: string;
};
