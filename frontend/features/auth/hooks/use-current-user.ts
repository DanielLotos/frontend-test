import { useMemo } from 'react';
import constate from 'constate';

import { useCurrentUserQuery } from './use-current-user-query';
import { useSignOutCleanup } from './use-sign-out-cleanup';

const useCurrentUserImpl = () => {
  const cleanup = useSignOutCleanup();

  const { data, isSuccess, isError, isFetched } = useCurrentUserQuery(null, {
    onError: err => {
      if (err.statusCode === 403) {
        cleanup();
      }
    },
  });

  const isLoaded = isFetched || isSuccess || isError;

  return useMemo(() => ({ currentUser: data?.payload, isLoaded }), [data?.payload, isLoaded]);
};

export const [CurrentUserProvider, useCurrentUser] = constate(useCurrentUserImpl);
