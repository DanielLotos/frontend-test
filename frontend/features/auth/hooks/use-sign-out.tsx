import { useMutation } from '@tanstack/react-query';

import { signOutReq } from '../api';
import { useSignOutCleanup } from './use-sign-out-cleanup';

export const useSignOut = () => {
  const cleanup = useSignOutCleanup();

  return useMutation(signOutReq, {
    onSuccess: cleanup,
  });
};
