import { NextSeo } from 'next-seo';

import { withPageAuth } from '../hocs';
import { SignInForm } from '../organisms/sign-in-form';
import { getAuthPageLayout } from '../templates';

const SignIn: NextPage = () => {
  return (
    <>
      <NextSeo title="Sign in" />
      <SignInForm />
    </>
  );
};

SignIn.getLayout = getAuthPageLayout;

export const SignInPage = withPageAuth({ pageType: 'publicOnly' }, () => '/')(SignIn);
