import { useCallback } from 'react';
import Link from 'next/link';
import { boolean, string, z } from 'zod';

import { Button, CheckboxField, Icon, PasswordField, TextButton, TextField } from '@sh/ui';

import { SignInInput } from '../dto';
import { useSignIn } from '../hooks';

const signInSchema = z.object({
  email: string().min(1).email(),
  password: string().min(6),
  rememberMe: boolean().optional(),
});

export const SignInForm = () => {
  const { mutateAsync } = useSignIn();
  const onSubmit = useCallback((input: SignInInput) => mutateAsync(input), [mutateAsync]);

  return (
    <section className="flex min-h-viewport flex-col bg-bg100 pt-20 shadow-hard-shadow sm:mb-12 sm:min-h-auto sm:w-full sm:max-w-narrow sm:pt-5 lg:mb-0 lg:min-h-viewport lg:max-w-full xl:min-h-auto xl:max-w-narrow">
      <div className="px-2 sm:px-6">
        <form className="grid">
          <h1 className="v-h700 mb-2 text-center sm:text-left">Sign in</h1>
          <TextField
            className="mt-12px"
            label={<span className="sr-only">Email</span>}
            name="email"
            placeholder="E-mail"
            type="email"
          />
          <PasswordField
            className="mt-12px"
            name="password"
            placeholder="Create a password"
            size="200"
            withIcon={false}
          />

          <Button
            className="mb-3 mt-12px"
            size="300"
            onClick={() => {
              onSubmit({ email: 'shakuro@example.com', password: 'shakuro' });
            }}
          >
            Sign in
          </Button>
          <Link href="#out-of-scope" legacyBehavior passHref>
            <TextButton as="a" className="mb-4" variant="primary">
              Forgot password?
            </TextButton>
          </Link>

          <CheckboxField
            className="mb-6 sm:mb-3"
            label="Remember me"
            name="rememberMe"
            type="checkbox"
          />
        </form>
      </div>
      <div className="v-btn200 mt-auto bg-bg-hover200 px-2 py-2 text-center sm:px-6">
        <Link href="#out-of-scope" legacyBehavior passHref>
          <TextButton as="a" variant="primary">
            Create account
            <Icon name="link-right" />
          </TextButton>
        </Link>
      </div>
    </section>
  );
};
