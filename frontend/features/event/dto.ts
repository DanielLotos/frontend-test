import { CategoryDTO } from 'features/category/dto';
import { LocationDTO } from 'features/shared';

import { UserShortDTO } from '../user';

export type EventParams = {
  categoryId?: number;
  name?: number;
  page?: number;
  size?: number;
};

export type EventDTO = {
  id: number;
  name: string;
  coverUrl: string;
  eventDate: string;
  timeZone?: string;
  location?: LocationDTO;
  experts: UserShortDTO[];
  description?: string;
};

export type EventDetailsDTO = EventDTO & { description?: string; categoryInfo: CategoryDTO };

export type EventMetaDTO = {
  page: number;
  size: number;
  totalPages: number;
  totalCount: number;
  isEmpty: true;
  isNotEmpty: true;
};
