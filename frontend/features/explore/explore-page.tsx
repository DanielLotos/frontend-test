import { addServerState, createQueryClient } from '@sh/core';

import { withPageAuth } from 'features/auth';
import { CategoryCardsList, useCategoriesQuery } from 'features/category';
import { useRecommendedExpertsQuery } from 'features/expert';

import { EventsSection } from './organisms/events-section';
import { RecommendedExperts } from './organisms/recommended-experts';

const ITEMS_TO_LOAD = 5;
const contentPxClassName = 'px-2 sm:px-3 md:px-4 lg:px-6 xl:px-7';

const Explore: NextPage = () => {
  return (
    <div className="space-y-5 py-3 sm:py-5 xl:py-7">
      <EventsSection className={contentPxClassName} itemsCountToLoad={ITEMS_TO_LOAD} />
      <RecommendedExperts className={contentPxClassName} itemsCountToLoad={ITEMS_TO_LOAD} />
      <CategoryCardsList className={contentPxClassName} />
    </div>
  );
};

Explore.getInitialProps = async ctx => {
  if (typeof window === 'object') return {};

  const client = createQueryClient(ctx);

  await Promise.all([
    client.prefetchQuery(useRecommendedExpertsQuery.getKey({ expertsTopCount: ITEMS_TO_LOAD })),
    client.prefetchQuery(useCategoriesQuery.getKey({})),
  ]);

  return addServerState(client);
};

export const ExplorePage = withPageAuth([{ pageType: 'privateOnly' }, { pageType: 'publicOnly' }])(
  Explore,
);
