import { createUseQueryHook } from '@sh/core';

import { CategoryDTO } from '../dto';

export const useCategoriesQuery = createUseQueryHook<
  { name?: string },
  APIListResponse<CategoryDTO>
>(params => ['/categories', { ...params, size: 1000 }, ['category']]);
