import { CategoryDTO } from 'features/category/dto';
import { LocationDTO } from 'features/shared';

export type UserRoles = 'User' | 'Expert';

export type UserDTO = {
  id: string;
  avatar?: string;
  email: string;
  emailConfirmed: boolean;
  expertPaymentSkipped?: boolean;
  expertStatus?: string;
  expertSubscriptionPriceCreated?: boolean;
  firstName?: string;
  lastName?: string;
  newsletter: boolean;
  roles: UserRoles[];
  signUpFlow: string;
  stripeAccount?: any;
  timezone: string;
};

export type UserShortDTO = Pick<UserDTO, 'id' | 'avatar' | 'firstName' | 'lastName'>;

export type UserProfileDTO = {
  id: ID;
  avatar?: string;
  cover?: string;
  dateOfBirth: string;
  expertAboutText: string;
  firstName: string;
  lastName?: string;
  location: LocationDTO;
  popularCategories?: CategoryDTO[];
  subscriptionCount?: number;
  priceInfo?: any;
  stripeAccount?: any;
  timezone: string;
};

export type UserProfileMetadata = {
  canBeSubscribed?: boolean;
  isInWatchList?: boolean;
  isSubscribedToExpert?: boolean;
  subscribedUntil?: DateTimeString;
};

export type UserPublicProfileDTO = {
  id: ID;
  avatar?: string;
  cover?: string;
  expertAboutText: string;
  firstName: string;
  lastName?: string;
  popularCategories?: CategoryDTO[];
  subscriptionCount?: number;
  priceInfo?: any;
  metadata?: UserProfileMetadata;
};

export type UserSubscriptionDTO = {
  id: ID;
  createdOn: DateTimeString;
  expert: UserShortDTO & { metadata?: UserProfileMetadata };
  status: any;
  priceInfo: any;
  startedAt?: DateTimeString;
  finishedAt?: DateTimeString;
};

export type ChangePasswordInput = {
  newPassword: string;
  confirmNewPassword: string;
};
