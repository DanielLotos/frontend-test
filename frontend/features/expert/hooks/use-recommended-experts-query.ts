import { createUseQueryHook } from '@sh/core';

import { UserPublicProfileDTO } from 'features/user';

export const useRecommendedExpertsQuery = createUseQueryHook<
  { expertsTopCount?: number; CategoryIds?: number[] },
  APIListResponse<UserPublicProfileDTO>
>(params => ['/experts/recommended', params, ['user']]);
