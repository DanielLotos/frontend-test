import { ReactNode } from 'react';
import Image from 'next/image';
import Link from 'next/link';
import clsx from 'clsx';

import { Avatar, Button, Dropdown, FullSizeLink, Icon } from '@sh/ui';

import { useCurrentUser } from 'features/auth';
import { getFullName, UserPublicProfileDTO } from 'features/user';

type ExpertCardProps = {
  profile?: UserPublicProfileDTO;
  className?: string;
  withCover?: boolean;
  withWatchlistButton?: boolean;
  withDropdown?: boolean;
  getTitle?: (profile?: UserPublicProfileDTO) => ReactNode;
};

export const ExpertCard = ({
  profile,
  className,
  withCover,
  withWatchlistButton,
  withDropdown,
  getTitle = profile => getFullName(profile),
}: ExpertCardProps) => {
  const name = getFullName(profile);
  const { currentUser } = useCurrentUser();

  return (
    <div className={clsx('relative', 'border border-outline500', className)}>
      {withCover && (
        <div className="-mx-1px -mt-1px aspect-[3.3862] bg-bg-disabled200">
          {profile?.cover && (
            <Image
              alt={`${name} cover image`}
              className="object-cover object-center size-full"
              height={640}
              src={profile.cover}
              width={640}
            />
          )}
        </div>
      )}

      <Link href={`/experts/${profile?.id}`} legacyBehavior passHref>
        <FullSizeLink
          className={clsx('block space-y-2 p-2', 'sm:p-3 md:p-4', withCover && '!pt-0 md:p-3')}
        >
          <Avatar
            className={clsx(
              'relative !size-8 md:!size-10',
              withCover && '-mt-6 border-2 border-txt100',
            )}
            size={80}
            src={profile?.avatar ?? ''}
            username={name}
          />
          <div className="flex items-start justify-between">
            <div className="flex flex-col gap-1">
              <h3 className="v-h500 truncate text-txt900">{getTitle(profile)}</h3>
              <p className="v-c300 truncate text-txt600">
                {profile?.popularCategories?.map(({ id, name }, index) => (
                  <span key={id}>
                    <span className="my-1 h-4px w-4px rounded-full bg-current" />
                    <span key={id}>{name}</span>
                    {index < (profile?.popularCategories?.length ?? 0) - 1 && <span>, </span>}
                  </span>
                ))}
              </p>
            </div>
            {!!currentUser && withDropdown && (
              <Dropdown.Root>
                <Dropdown.Trigger
                  as={Button}
                  className="z-10 rounded-none"
                  iconOnly
                  size="100"
                  variant="filled-white"
                >
                  <Icon name="dots" />
                </Dropdown.Trigger>
                <Dropdown.Content>
                  <Dropdown.Item
                    onClick={e => {
                      e.stopPropagation();
                    }}
                  >
                    <Icon
                      name={profile?.metadata?.isInWatchList ? 'filled-watchlist' : 'watchlist'}
                    />
                    {profile?.metadata?.isInWatchList ? 'Remove from' : 'Add to'} watchlist
                  </Dropdown.Item>
                  <Dropdown.Item>
                    <Icon name="dropdown-plus" /> Subscribe
                  </Dropdown.Item>
                </Dropdown.Content>
              </Dropdown.Root>
            )}
          </div>
          {profile?.expertAboutText && (
            <p className={clsx('v-p400 text-txt700', withCover && 'line-clamp-2')}>
              {profile.expertAboutText}
            </p>
          )}
          {withWatchlistButton && (
            <Button
              size="100"
              variant="filled"
              onClick={(e: any) => {
                e.stopPropagation();
                e.preventDefault();
              }}
            >
              <Icon name={profile?.metadata?.isInWatchList ? 'filled-watchlist' : 'watchlist'} />
              {profile?.metadata?.isInWatchList ? 'Remove from' : 'Add to'} watchlist
            </Button>
          )}
        </FullSizeLink>
      </Link>
    </div>
  );
};
