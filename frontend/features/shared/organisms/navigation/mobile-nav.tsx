import Link from 'next/link';
import clsx from 'clsx';

import { Avatar, Button, Dialog, Icon, SafeLink, TextButton, useDialogState } from '@sh/ui';

import { matchRoles, useCurrentUser } from 'features/auth';
import { getFullName } from 'features/user';

import { routes } from '../../constants';
import { Navigation } from './nav';

export const MobileNavMenu = () => {
  const { isOpen, toggleDialog, open, close } = useDialogState();
  const { currentUser } = useCurrentUser();

  return (
    <div className="flex items-center lg:hidden">
      <Dialog.Root open={isOpen} onOpenChange={toggleDialog}>
        <Button
          aria-label="Open navigation"
          className="hover:!bg-transparent"
          iconOnly
          size="100"
          variant="clear"
          onClick={open}
        >
          <Icon name="burger-menu" />
        </Button>
        <Dialog.Content
          className={clsx(
            'absolute !top-0 !m-0 w-full !max-w-full !rounded-[0] !bg-bg100',
            currentUser && 'h-full',
            'overflow-auto',
          )}
          disableAutoFocus
          rootClassName="z-header+ overflow-hidden"
          transitionClassName="motion-safe:d-state-closed:animate-slide-out-bottom motion-safe:d-state-open:animate-slide-in-bottom"
        >
          <div className="relative py-5">
            <Button
              aria-label="Close navigation"
              className="!absolute right-12px top-12px"
              iconOnly
              size="100"
              variant="clear"
              onClick={close}
            >
              <Icon name="close" />
            </Button>
            <div>
              <div className="flex flex-col items-center justify-center gap-12px border-outline300">
                {currentUser ? (
                  <>
                    <Avatar
                      alt="user avatar"
                      size={80}
                      src={currentUser?.avatar as unknown as string}
                      username={getFullName(currentUser)}
                    />
                    <div>
                      <p className="v-h600 mb-12px text-center">{getFullName(currentUser)}</p>
                      {matchRoles(['Expert'], currentUser) && (
                        <SafeLink
                          className="v-btn100 flex justify-center text-primary100"
                          href="/profile"
                        >
                          Open my profile
                        </SafeLink>
                      )}
                    </div>
                    <div className="mb-5">
                      {matchRoles(['Expert'], currentUser) ? (
                        <Button className="rounded-full" size="100" variant="primary">
                          New Post
                        </Button>
                      ) : (
                        <Link href={routes.partner} legacyBehavior passHref>
                          <Button as="a" className="rounded-full" size="100" variant="filled">
                            Partner with us
                          </Button>
                        </Link>
                      )}
                    </div>
                    <hr className="h-[2px] w-full border-0 bg-outline300" />
                  </>
                ) : (
                  <h2 className="v-h800 mb-3">Menu</h2>
                )}
                <div className="flex w-full flex-col px-4 pb-12px">
                  <Navigation close={close} location="mobile-nav" />
                  {currentUser && (
                    <>
                      <hr className={clsx('my-1 h-[2px] w-full border-0 bg-outline300')} />
                      <TextButton
                        className={clsx(
                          'v-nav320 cursor-pointer select-none items-center',
                          'flex h-5 w-full !justify-start gap-1',
                          'rounded outline-none',
                          '!text-error600',
                          'disabled:pointer-events-none disabled:text-content-disabled200',
                        )}
                        onClick={() => alert('TODO: implement logout')}
                      >
                        <span className="relative group-data-[state=opened]/sidebar:pointer-events-none">
                          <Icon name="log-out" />
                        </span>

                        <span className="transition-opacity data-[state=default]:delay-150 group-data-[state=collapsed]/sidebar:opacity-0">
                          Logout
                        </span>
                      </TextButton>
                    </>
                  )}
                </div>
              </div>
            </div>
          </div>
        </Dialog.Content>
      </Dialog.Root>
    </div>
  );
};
