import { FC } from 'react';
import clsx from 'clsx';
import { isEmpty, isNotNil, not } from 'ramda';

import { CollapsibleItem } from './collapsible-item';
import { NavItem } from './nav-item';
import { MenuLocation } from './types';
import { useMenuItems } from './use-items';

export type NavigationProps<T = MenuLocation> = T extends 'mobile-nav'
  ? {
      location: T;
      close: () => void;
    }
  : { location: T; close?: never };

export const Navigation: FC<NavigationProps> = ({ location, close }) => {
  const menuItems = useMenuItems(location);

  return (
    <ul
      className={clsx('flex w-full flex-col', location === 'mobile-nav' ? 'gap-12px' : 'gap-4px')}
    >
      {menuItems?.length > 0 &&
        menuItems.map(item => {
          const { label, styles, icon, itemData, counter, href, ...rest } = item;
          const clickHandler = () => close && close();

          return (
            <li key={item.id} className="flex w-full items-center justify-between" {...rest}>
              {isNotNil(itemData) && not(isEmpty(itemData)) ? (
                <>
                  <CollapsibleItem
                    counter={counter}
                    icon={icon}
                    itemData={itemData}
                    label={label}
                    styles={styles}
                  />
                </>
              ) : (
                <NavItem
                  counter={counter}
                  href={href || ''}
                  icon={icon}
                  label={label}
                  styles={styles}
                  onClick={clickHandler}
                />
              )}
            </li>
          );
        })}
    </ul>
  );
};
