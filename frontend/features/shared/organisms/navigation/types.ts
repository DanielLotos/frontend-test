import { IconsType } from '@sh/ui/atoms/icon/types';

import { UserRoles } from 'features/user';

export type ItemData = {
  id?: string;
  icon?: IconsType;
  label: string;
  counter?: number;
  href: string;
  image: string;
};

export type ItemForMenu = {
  id?: string;
  icon?: IconsType;
  label: string;
  counter?: number;
  itemData?: ItemData[];
  divider?: boolean;
  styles?: string;
  href?: string;
  onClick?: () => void;
};

export type MenuItem = {
  id: string;
  styles?: string;
  href: string;
  icon: IconsType;
  label: string;
  userRole: UserRoles[];
  location: MenuLocation[];
  itemData?: [];
  counter?: number;
};

export type MenuLocation = 'sidebar' | 'mobile-nav';
