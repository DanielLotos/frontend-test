export const favicon = (
  <>
    <link href="/apple-touch-icon.png" rel="apple-touch-icon" sizes="180x180" />
    <link href="/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png" />
    <link href="/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png" />
    <link href="/site.webmanifest" rel="manifest" />
    <link color="#6B15D4" href="/safari-pinned-tab.svg" rel="mask-icon" />
    <meta content="TestTask" name="apple-mobile-web-app-title" />
    <meta content="TestTask" name="application-name" />
    <meta content="#6B15D4" name="msapplication-TileColor" />
    <meta content="#6B15D4" name="theme-color" />
  </>
);
