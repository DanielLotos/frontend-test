export type LocationDTO = {
  country?: string;
  state?: string;
  city?: string;
  address?: string;
};
