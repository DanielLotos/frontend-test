// @ts-check
import { getConfig } from '../core/libs/next-image/get-config.mjs';

// TODO: provide image sizes depending on your project needs
export const images = getConfig({
  deviceSizes: [600, 980, 1200, 1600, 1960, 2400, 3200],
  imageSizes: [24, 32, 48, 64, 96, 128, 256, 384],
});
