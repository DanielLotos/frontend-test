/* eslint-disable @typescript-eslint/no-var-requires */
const path = require('path');
const distDirectory = path.join(__dirname, '../');
const templateDirectory = path.join(__dirname, './');

module.exports = (plop) => {
  plop.setGenerator('feature', {
    description: 'Create a new feature (basic only)',
    prompts: [
      {
        type: 'input',
        name: 'feature',
        message: 'What is your feature name (will be transformed to kebab-case)?',
      },
    ],
    actions: [
      {
        type: 'add',
        path: `${distDirectory}/features/{{kebabCase feature}}/index.ts`,
        templateFile: `${templateDirectory}/feature.plop`,
      },
    ],
  });

  plop.setGenerator('lib', {
    description: 'Create a new lib',
    prompts: [
      {
        type: 'input',
        name: 'lib',
        message: 'What is your library name (will be transformed to kebab-case)?',
      },
    ],
    actions: [
      {
        type: 'add',
        path: `${distDirectory}/libs/{{kebabCase lib}}/index.ts`,
        templateFile: `${templateDirectory}/lib.plop`,
      },
      {
        type: 'add',
        path: `${distDirectory}/libs/{{kebabCase lib}}/{{kebabCase lib}}.stories.mdx`,
        templateFile: `${templateDirectory}/lib.stories.plop`,
      },
      {
        type: 'add',
        path: `${distDirectory}/libs/{{kebabCase lib}}/{{kebabCase lib}}.test.ts`,
        templateFile: `${templateDirectory}/lib.test.plop`,
      },
      {
        type: 'append',
        path: `${distDirectory}/libs/index.ts`,
        separator: '',
        template: `export * from './{{kebabCase lib}}';\n`,
      },
    ],
  });
};
