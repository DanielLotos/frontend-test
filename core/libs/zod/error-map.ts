import { path } from 'ramda';
import { defaultErrorMap, ZodErrorMap, ZodIssueCode, ZodParsedType } from 'zod';

import { zodDefaultLocale } from './default-locale';

const jsonStringifyReplacer = (_: string, value: any): any => {
  if (typeof value === 'bigint') {
    return value.toString();
  }

  return value;
};

function joinValues<T extends any[]>(array: T, separator = ' | '): string {
  return array.map(val => (typeof val === 'string' ? `'${val}'` : val)).join(separator);
}

const isRecord = (value: unknown): value is Record<string, unknown> => {
  if (typeof value !== 'object' || value === null) return false;

  for (const key in value) {
    if (!Object.prototype.hasOwnProperty.call(value, key)) return false;
  }

  return true;
};

const getKeyAndValues = (
  param: unknown,
  defaultKey: string,
): {
  values: Record<string, unknown>;
  key: string;
} => {
  if (typeof param === 'string') return { key: param, values: {} };

  if (isRecord(param)) {
    const key = 'key' in param && typeof param.key === 'string' ? param.key : defaultKey;

    const values = 'values' in param && isRecord(param.values) ? param.values : {};

    return { key, values };
  }

  return { key: defaultKey, values: {} };
};

export const interpolate = (
  locale: typeof zodDefaultLocale,
  key: string,
  options?: Record<string, any> | undefined,
) => {
  let match: RegExpExecArray | null;
  const regexp = new RegExp('{{(.+?)}}', 'g');

  let str = path(key.split('.'))(locale) as string;
  if (str) {
    while ((match = regexp.exec(str))) {
      const matchedVar = match[1].trim();
      const value = options && options[matchedVar];
      str = str.replace(match[0], value);
    }
  }

  return str;
};

export type MakeZodI18nMap = (locale: typeof zodDefaultLocale) => ZodErrorMap;

export const createZodErrorMap: MakeZodI18nMap = locale => (issue, ctx) => {
  let message: string;
  message = defaultErrorMap(issue, ctx).message;

  switch (issue.code) {
    case ZodIssueCode.invalid_type:
      if (issue.received === ZodParsedType.undefined) {
        message =
          ctx.defaultError === message
            ? interpolate(locale, 'errors.invalid_type_received_undefined') || message
            : ctx.defaultError;
      } else {
        message =
          interpolate(locale, 'errors.invalid_type', {
            expected: interpolate(locale, `types.${issue.expected}`) || issue.expected,
            received: interpolate(locale, `types.${issue.received}`) || issue.received,
          }) || message;
      }
      break;
    case ZodIssueCode.invalid_literal:
      message =
        interpolate(locale, 'errors.invalid_literal', {
          expected: JSON.stringify(issue.expected, jsonStringifyReplacer),
        }) || message;
      break;
    case ZodIssueCode.unrecognized_keys:
      message =
        interpolate(locale, 'errors.unrecognized_keys', {
          keys: joinValues(issue.keys, ', '),
          count: issue.keys.length,
        }) || message;
      break;
    case ZodIssueCode.invalid_union:
      message = interpolate(locale, 'errors.invalid_union') || message;
      break;
    case ZodIssueCode.invalid_union_discriminator:
      message =
        interpolate(locale, 'errors.invalid_union_discriminator', {
          options: joinValues(issue.options),
        }) || message;
      break;
    case ZodIssueCode.invalid_enum_value:
      message =
        interpolate(locale, 'errors.invalid_enum_value', {
          options: joinValues(issue.options),
          received: issue.received,
        }) || message;
      break;
    case ZodIssueCode.invalid_arguments:
      message = interpolate(locale, 'errors.invalid_arguments') || message;
      break;
    case ZodIssueCode.invalid_return_type:
      message = interpolate(locale, 'errors.invalid_return_type') || message;
      break;
    case ZodIssueCode.invalid_date:
      message = interpolate(locale, 'errors.invalid_date') || message;
      break;
    case ZodIssueCode.invalid_string:
      if (typeof issue.validation === 'object') {
        if ('startsWith' in issue.validation) {
          message =
            interpolate(locale, 'errors.invalid_string.startsWith', {
              startsWith: issue.validation.startsWith,
            }) || message;
        } else if ('endsWith' in issue.validation) {
          message =
            interpolate(locale, 'errors.invalid_string.endsWith', {
              endsWith: issue.validation.endsWith,
            }) || message;
        }
      } else {
        message =
          interpolate(locale, `errors.invalid_string.${issue.validation}`, {
            validation: interpolate(locale, `validations.${issue.validation}`) || issue.validation,
          }) || message;
      }
      break;
    case ZodIssueCode.too_small:
      const minimum =
        issue.type === 'date'
          ? new Date(issue.minimum as number).toLocaleDateString()
          : issue.minimum;

      message =
        interpolate(
          locale,
          `errors.too_small.${issue.type}.${
            issue.exact ? 'exact' : issue.inclusive ? 'inclusive' : 'not_inclusive'
          }`,
          {
            minimum,
            count: typeof minimum === 'number' ? minimum : undefined,
          },
        ) || message;
      break;
    case ZodIssueCode.too_big:
      const maximum =
        issue.type === 'date'
          ? new Date(issue.maximum as number).toLocaleDateString()
          : issue.maximum;

      message =
        interpolate(
          locale,
          `errors.too_big.${issue.type}.${
            issue.exact ? 'exact' : issue.inclusive ? 'inclusive' : 'not_inclusive'
          }`,
          {
            maximum,
            count: typeof maximum === 'number' ? maximum : undefined,
          },
        ) || message;
      break;
    case ZodIssueCode.custom:
      const { key, values } = getKeyAndValues(issue.params?.i18n, 'errors.custom');
      message =
        issue.message ||
        interpolate(locale, key, {
          ...values,
        }) ||
        message;
      break;
    case ZodIssueCode.invalid_intersection_types:
      message = interpolate(locale, 'errors.invalid_intersection_types') || message;
      break;
    case ZodIssueCode.not_multiple_of:
      message =
        interpolate(locale, 'errors.not_multiple_of', {
          multipleOf: issue.multipleOf,
        }) || message;
      break;
    case ZodIssueCode.not_finite:
      message = interpolate(locale, 'errors.not_finite') || message;
      break;
    default:
  }

  return { message };
};
