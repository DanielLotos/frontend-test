import { useFieldArray, useForm } from 'react-hook-form';
import { Canvas, Meta, Story } from '@storybook/addon-docs';
import { z } from 'zod';

import { Button, CheckboxField, FieldError, Form, Icon, TextField } from '@sh/ui';

import { sleep } from '../sleep';
import { zodResolver } from './resolver';
import { chosen, someValidator } from './validators';

<Meta title="core | libs/zod" />

# Validation with zod

## Schema and validators

for form validation zod schema should be created

```tsx
const schema = z.object({
  email: z.string().email(),
});
```

For form validation the most used types are string, date, number and boolean. All possible values you can look here
[zod parsed types](https://zod.dev/ERROR_HANDLING?id=zodparsedtype)

Zod has validators for build in types.

- [string-specific validations](https://zod.dev/?id=strings)
- [number-specific validations](https://zod.dev/?id=numbers)
- [date-specific validations](https://zod.dev/?id=dates)

You can create custom validator with [.refine](https://zod.dev/?id=refine) method and chain it after builtin one

```tsx
const schema = z.object({
  email: z
    .string()
    .email()
    .refine(val => val.length <= 255, {
      message: "String can't be more than 255 characters",
    }),
});
```

or this way

```tsx
type ErrorMapParams = {
  [key: string]: any;
};

type CustomValidatorParams = {
  message?: string;
  path?: (string | number)[];
  params?: ErrorMapParams; //for error map
};

const customValidator = (config?: CustomValidatorParams) => {
  const isCustom = (value: unknown) => {
    return (value as string).length <= 255;
  };

  const params: CustomValidatorParams = {
    message: "String can't be more than 255 characters",
    ...config,
  };

  return [isCustom, params] as const;
};

const schema = z.object({
  email: z
    .string()
    .email()
    .refine(...someValidator()),
});
```

if you need validate linked fields like password and confirm password you can you .refine like this

```tsx
const schema = z
  .object({
    password: z.string(),
    confirm: z.string(),
  })
  .refine(data => data.password === data.confirm, {
    message: "Passwords don't match",
    path: ['confirm'], // with path you can assign on which field to show error message
  });
```

- you could install validator.js and use their validations with Zod. Zod's refine function makes this fairly straightforward. For example if you wanted to validate a string as a credit card number using Zod and validator it might look something like this

```tsx
import { z } from 'zod';
import isCreditCard from 'validator/lib/isCreditCard';

const userSchema = z.object({
  name: z.string(),
  creditCard: z.string().refine(isCreditCard, {
    message: 'Must be a valid credit card number',
  }),
});
```

> ⚠️ ⚠️ ⚠️ Check out [validator.js](https://github.com/validatorjs/validator.js) for a bunch of other useful string validation functions that can be used in conjunction with [.refine](https://zod.dev/?id=refine)

- if you need validate image with zod you can do it like this

```tsx
const userSchema = z.object({
  avatar: z
    .custom<FileList>()
    .transform(file => file.length > 0 && file.item(0))
    .refine(file => !file || (!!file && file.size <= 10 * 1024 * 1024), {
      message: 'The profile picture must be a maximum of 10MB.',
    })
    .refine(file => !file || (!!file && file.type?.startsWith('image')), {
      message: 'Only images are allowed to be sent.',
    }),
});
```

- if you need validate value that should be limited to only a few possible values (like Yup: Yup.string().oneOf([5, 10, 15]))

```tsx
const schema = z.object({
  field: z.union([z.literal(5), z.literal(10), z.literal(15)]),
});
```

- if you need validate string input value as number you can coerse type before validation

```tsx
age: z.preprocess(
  a => parseInt(z.string().parse(a), 10),
  z.number().gte(18, 'Must be 18 and above'),
);

//or
age: z.coerce.number().gte(18, 'Must be 18 and above');
```

## Error messages

Errors in zod can be customized with ZodErrorMap
toolbox error map was based on [zod-i18n](https://github.com/aiji42/zod-i18n), so if you need i18n use this lib

Custom error map can be applied globally

```tsx
import { createZodErrorMap, zodDefaultLocale } from '@sh/core';

z.setErrorMap(createZodErrorMap(zodDefaultLocale));
```

or for specific form

```tsx
import { zodResolver } from '@sh/core';

const schema = z.object({
  email: z.string().email(),
});

export const ForgotPasswordForm = () => {
  const methods = useForm({ resolver: zodResolver(schema) });
  //...
};
```

you can customize error messages in several ways

- default error messages for builtin validations you can change in default-locale.ts

```tsx
export const zodDefaultLocale: Record<string, any> = {
  errors: {
    //...
    invalid_type_received_undefined: 'Required', // change to 'Required field'
    //...
  },
};
```

- for specific field you can add .message in validation params

```tsx
const schema = z.object({
  email: z.string().email({ message: 'Custom error message' }),
});
```

- for custom validations you can add can use params of .refine

```tsx
const schema = z.object({
  email: z
    .string()
    .email()
    .refine(val => val.length <= 255, {
      message: "String can't be more than 255 characters",
    }),
});
```

- you can add totally custom validation and error messages at once with [.superRefine](https://zod.dev/?id=superrefine)

```tsx
const schema = z
  .object({
    password: z.string(),
    confirm: z.string(),
  })
  .superRefine((val, ctx) => {
    if (val.length > 3) {
      ctx.addIssue({
        code: z.ZodIssueCode.too_big,
        maximum: 3,
        type: 'array',
        inclusive: true,
        message: 'Too many items 😡',
      });
    }

    if (val.length !== new Set(val).size) {
      ctx.addIssue({
        code: z.ZodIssueCode.custom,
        message: `No duplicates allowed.`,
      });
    }
  });
```

zodIssue has this [type](https://zod.dev/ERROR_HANDLING?id=zodissue) and for each ZodIssueCode exist various params that you can find [here](https://zod.dev/ERROR_HANDLING?id=zodissuecode)

```tsx
type ZodeIssue = {
  code: z.ZodIssueCode;
  path: (string | number)[];
  message: string;
};
```

You can create new errorMap from scratch. It is a function that takes two params and return object with message. Default zod error map you can look [here](https://github.com/colinhacks/zod/blob/master/src/locales/en.ts)

```tsx
import * as z from 'zod';
import { zodResolver } from '@hookform/resolvers/zod';

const customErrorMap: z.ZodErrorMap = (error, ctx) => {
  /*
  This is where you override the various error codes
  */
  switch (error.code) {
    case z.ZodIssueCode.invalid_type:
      if (error.expected === 'string') {
        return { message: `This ain't a string!` };
      }
      break;
    case z.ZodIssueCode.custom:
      // produce a custom message using error.params
      // error.params won't be set unless you passed
      // a `params` arguments into a custom validator
      const params = error.params || {};
      if (params.myField) {
        return { message: `Bad input: ${params.myField}` };
      }
      break;
  }

  // fall back to default message!
  return { message: ctx.defaultError };
};

const schema = z.object({
  age: z.number(),
});

export const ForgotPasswordForm = () => {
  const methods = useForm({ resolver: zodResolver(schema, { errorMap: customErrorMap }) });
  //...
};
```

> ⚠️ ⚠️ ⚠️ You can validate values at any moment, but result should be resolved by you. More about it [here](https://zod.dev/ERROR_HANDLING?id=error-handling-for-forms)

```tsx
const schema = z.object({
  email: z
    .string()
    .email()
    .refine(val => val.length <= 255, {
      message: "String can't be more than 255 characters",
    }),
});

//somewhere later
const validationResult = schema.safeParse(formData);
if (!validationResult.success) {
  console.log(validationResult.error.issues);
}

/*
  [
    {
      "validation": "email",
      "code": "invalid_string",
      "message": "Invalid email",
      "path": ["contactInfo","email"]
    }
  ]
*/
```

### Form example

Form schema for debug

```tsx
const schema = z
  .object({
    email: z
      .string()
      .email()
      .refine(...someValidator()),
    name: z.string().optional(),
    count: z.number().positive({ message: 'Number should be positive!' }),
    subscribe: z.boolean().refine(...chosen({ message: 'you should subscribe to continue' })),
    gender: z.string({ required_error: 'are you from lgbtk+?' }).refine(data => data === 'male', {
      message: 'Are you sure that you are male?',
    }),
    socials: z
      .object({
        messenger: z.string().min(5),
        username: z.string(),
      })
      .array()
      .nonempty()
      .max(3),
  })
  .refine(data => data.gender === 'male' && data.subscribe, {
    message: 'Only subscribed male are permitted to submit!',
    path: ['subscribe'],
  })
  .superRefine((val, ctx) => {
    //here you can validate multiple fields and add multiple errors
    ctx.addIssue({
      code: z.ZodIssueCode.custom,
      message: 'Check email one more time',
      path: ['email'],
    });
    ctx.addIssue({
      code: z.ZodIssueCode.custom,
      message: 'Forgot about a name, its a joke that this field is optional',
      path: ['root'],
    });

    return z.NEVER;
  });
```

<Canvas>
  <Story args={{ className: '',}} name="Form">
	{() => {
    	const schema = z.object({
        email: z.string().email().refine(...someValidator()),
        name: z.string().optional(),
        count: z.number().positive({message: 'Number should be positive!'}),
        subscribe: z.boolean().refine(...chosen({message: 'you should subscribe to continue'})),
        gender: z.string({required_error: 'are you from lgbtk+?'}).refine(data => data === 'male', {
            message: 'Are you sure that you are male?',
          }),
        socials: z.object({
          messenger: z.string().min(5),
          username: z.string()
        }).array().nonempty().max(3),
        }).refine(data => data.gender === 'male' && data.subscribe, {
              message: 'Only subscribed male are permitted to submit!',
              path:['subscribe']
        }).superRefine((val, ctx) => {
            //here you can validate multiple fields and add multiple errors
            if(val.email !== 'test@test.com') {ctx.addIssue({
              code: z.ZodIssueCode.custom,
              message: 'Type test@test.com',
              path: ['email']
            });}

            ctx.addIssue({
              code: z.ZodIssueCode.custom,
              message: 'Forgot about a name, its a joke that this field is optional',
              path: ['root']});

            return z.NEVER;
      });

      //mode: onChange | onBlur | onSubmit | onTouched | all = 'onSubmit'
      //reValidateMode: onChange | onBlur | onSubmit = 'onChange'
    	const methods = useForm({ resolver: zodResolver(schema), mode: 'onChange'});

      const { fields, remove, append } = useFieldArray({
        control: methods.control,
        name:'socials',
      });

    	const requestPasswordReq = (...args) => new Promise(async (res, rej) => {
    		await sleep(1000);
        // eslint-disable-next-line no-console
    		console.log(args);

    		if (Math.random() > 0.3) {
    			res();
    		} else if (Math.random() > 0.6) {
    			rej({ errors: { email: 'email is taken' } });
    		} else {
    			rej({ errors: {}, error: 'global error' });
    		}
    	});

    	return (
    		<Form.Root {...methods}>
    			<Form.Content className="grid gap-3" onSubmit={requestPasswordReq}>
    				<h1>Test form</h1>

    				<Form.Field
    				component={TextField}
    				label="Email"
    				name="email"
    				placeholder="Enter small size email"
    				type="email"
    				/>
    				<Form.Field component={TextField} name="name" placeholder="Enter name" type="text" />
    				<Form.Field component={TextField} name="count" placeholder="Enter number" type="number" />
    				<Form.Field
    				component={CheckboxField}
    				label="Subscribe to newsletter?"
    				name="newsletter"
    				type="checkbox"
    				/>
            <div className="relative">
    				<Form.Field
    				component={CheckboxField}
    				label="Subscribe to newsletter?"
    				name="subscribe"
    				type="switch"
    				/>
            </div>
    				<div className="relative flex gap-2">
    				<Form.Field
    					component={CheckboxField}
    					label="Male"
    					name="gender"
    					type="radio"
    					value="male"
    				/>
    				<Form.Field
    					component={CheckboxField}
    					label="Female"
    					name="gender"
    					type="radio"
    					value="female"
    				/>

    				</div>
            <div>
              <section className="relative">
                <ul className="flex flex-wrap gap-1">
                  {fields.map((item, index) => (
                    <li key={item.id} className="flex w-full items-center gap-1">
                      <Form.Field
                        component={TextField}
                        label="messenger"
                        name={`socials.${index}.messenger`}
                        placeholder="Enter messenger"
                        type="text"
                      />
                      <Form.Field
                        component={TextField}
                        label="username"
                        name={`socials.${index}.username`}
                        placeholder="Enter username"
                        type="text"
                      />
                      <Icon
                        className="cursor-pointer text-error200"
                        name="del"
                        size={24}
                        onClick={() => remove(index)}
                      />
                    </li>
                  ))}
                </ul>
                {methods?.formState?.errors?.socials?.message && <FieldError>{methods.formState.errors?.socials?.message}</FieldError>}
                <div className="text-success100">
                  <Icon
                    className="cursor-pointer"
                    name="plus"
                    size={24}
                    onClick={() => append({messenger: 'VK', username: 'Vasya'})}
                  />{' '}
                  Add messenger
                </div>
              </section>
            </div>
    				<Form.GlobalError variant="primary" />

    				<Form.Submit as={Button}>Send instructions</Form.Submit>
    			</Form.Content>
    		</Form.Root>
    	);
    }}

  </Story>
</Canvas>
