/* eslint-disable max-lines */
import { SafeParseReturnType, z, ZodError } from 'zod';

import { zodDefaultLocale } from './default-locale';
import { createZodErrorMap, interpolate } from './error-map';

beforeAll(() => {
  z.setErrorMap(createZodErrorMap(zodDefaultLocale));
});

test('string parser error messages', () => {
  const schema = z.string();

  expect(getErrorMessage(schema.safeParse(undefined))).toEqual(
    zodDefaultLocale.errors.invalid_type_received_undefined,
  );
  expect(getErrorMessage(schema.safeParse(1))).toEqual(
    interpolate(zodDefaultLocale, 'errors.invalid_type', {
      expected: zodDefaultLocale.types.string,
      received: zodDefaultLocale.types.number,
    }),
  );

  expect(getErrorMessage(schema.safeParse(true))).toEqual(
    interpolate(zodDefaultLocale, 'errors.invalid_type', {
      expected: zodDefaultLocale.types.string,
      received: zodDefaultLocale.types.boolean,
    }),
  );
  expect(getErrorMessage(schema.safeParse(Date))).toEqual(
    interpolate(zodDefaultLocale, 'errors.invalid_type', {
      expected: zodDefaultLocale.types.string,
      received: zodDefaultLocale.types.function,
    }),
  );
  expect(getErrorMessage(schema.safeParse(new Date()))).toEqual(
    interpolate(zodDefaultLocale, 'errors.invalid_type', {
      expected: zodDefaultLocale.types.string,
      received: zodDefaultLocale.types.date,
    }),
  );
  expect(getErrorMessage(schema.email().safeParse(''))).toEqual(
    interpolate(zodDefaultLocale, 'errors.invalid_string.email', {
      validation: zodDefaultLocale.validations.email,
    }),
  );
  expect(getErrorMessage(schema.url().safeParse(''))).toEqual(
    interpolate(zodDefaultLocale, 'errors.invalid_string.url', {
      validation: zodDefaultLocale.validations.url,
    }),
  );
  expect(getErrorMessage(schema.regex(/aaa/).safeParse(''))).toEqual(
    interpolate(zodDefaultLocale, 'errors.invalid_string.regex', {
      validation: zodDefaultLocale.validations.regex,
    }),
  );
  expect(getErrorMessage(schema.startsWith('foo').safeParse(''))).toEqual(
    interpolate(zodDefaultLocale, 'errors.invalid_string.startsWith', {
      startsWith: 'foo',
    }),
  );
  expect(getErrorMessage(schema.endsWith('bar').safeParse(''))).toEqual(
    interpolate(zodDefaultLocale, 'errors.invalid_string.endsWith', {
      endsWith: 'bar',
    }),
  );
  expect(getErrorMessage(schema.min(5).safeParse('a'))).toEqual(
    interpolate(zodDefaultLocale, 'errors.too_small.string.inclusive', {
      minimum: 5,
    }),
  );
  expect(getErrorMessage(schema.max(5).safeParse('abcdef'))).toEqual(
    interpolate(zodDefaultLocale, 'errors.too_big.string.inclusive', {
      maximum: 5,
    }),
  );
  expect(getErrorMessage(schema.length(5).safeParse('abcdef'))).toEqual(
    interpolate(zodDefaultLocale, 'errors.too_small.string.exact', {
      minimum: 5,
    }),
  );
  expect(getErrorMessage(schema.datetime().safeParse('2020-01-01T00:00:00+02:00'))).toEqual(
    interpolate(zodDefaultLocale, 'errors.invalid_string.datetime', {
      validation: zodDefaultLocale.validations.datetime,
    }),
  );
});

test('number parser error messages', () => {
  const schema = z.number();

  expect(getErrorMessage(schema.safeParse(undefined))).toEqual(
    zodDefaultLocale.errors.invalid_type_received_undefined,
  );
  expect(getErrorMessage(schema.safeParse(''))).toEqual(
    interpolate(zodDefaultLocale, 'errors.invalid_type', {
      expected: zodDefaultLocale.types.number,
      received: zodDefaultLocale.types.string,
    }),
  );
  expect(getErrorMessage(schema.safeParse(null))).toEqual(
    interpolate(zodDefaultLocale, 'errors.invalid_type', {
      expected: zodDefaultLocale.types.number,
      received: zodDefaultLocale.types.null,
    }),
  );
  expect(getErrorMessage(schema.safeParse(NaN))).toEqual(
    interpolate(zodDefaultLocale, 'errors.invalid_type', {
      expected: zodDefaultLocale.types.number,
      received: zodDefaultLocale.types.nan,
    }),
  );
  expect(getErrorMessage(schema.int().safeParse(0.1))).toEqual(
    interpolate(zodDefaultLocale, 'errors.invalid_type', {
      expected: zodDefaultLocale.types.integer,
      received: zodDefaultLocale.types.float,
    }),
  );
  expect(getErrorMessage(schema.multipleOf(5).safeParse(2))).toEqual(
    interpolate(zodDefaultLocale, 'errors.not_multiple_of', {
      multipleOf: 5,
    }),
  );
  expect(getErrorMessage(schema.step(0.1).safeParse(0.0001))).toEqual(
    interpolate(zodDefaultLocale, 'errors.not_multiple_of', {
      multipleOf: 0.1,
    }),
  );
  expect(getErrorMessage(schema.lt(5).safeParse(10))).toEqual(
    interpolate(zodDefaultLocale, 'errors.too_big.number.not_inclusive', {
      maximum: 5,
    }),
  );
  expect(getErrorMessage(schema.lte(5).safeParse(10))).toEqual(
    interpolate(zodDefaultLocale, 'errors.too_big.number.inclusive', {
      maximum: 5,
    }),
  );
  expect(getErrorMessage(schema.gt(5).safeParse(1))).toEqual(
    interpolate(zodDefaultLocale, 'errors.too_small.number.not_inclusive', {
      minimum: 5,
    }),
  );
  expect(getErrorMessage(schema.gte(5).safeParse(1))).toEqual(
    interpolate(zodDefaultLocale, 'errors.too_small.number.inclusive', {
      minimum: 5,
    }),
  );
  expect(getErrorMessage(schema.nonnegative().safeParse(-1))).toEqual(
    interpolate(zodDefaultLocale, 'errors.too_small.number.inclusive', {
      minimum: 0,
    }),
  );
  expect(getErrorMessage(schema.nonpositive().safeParse(1))).toEqual(
    interpolate(zodDefaultLocale, 'errors.too_big.number.inclusive', {
      maximum: 0,
    }),
  );
  expect(getErrorMessage(schema.negative().safeParse(1))).toEqual(
    interpolate(zodDefaultLocale, 'errors.too_big.number.not_inclusive', {
      maximum: 0,
    }),
  );
  expect(getErrorMessage(schema.positive().safeParse(0))).toEqual(
    interpolate(zodDefaultLocale, 'errors.too_small.number.not_inclusive', {
      minimum: 0,
    }),
  );
  expect(getErrorMessage(schema.finite().safeParse(Infinity))).toEqual(
    zodDefaultLocale.errors.not_finite,
  );
});

test('date parser error messages', async () => {
  const testDate = new Date('2022-08-01');
  const schema = z.date();

  expect(getErrorMessage(schema.safeParse('2022-12-01'))).toEqual(
    interpolate(zodDefaultLocale, 'errors.invalid_type', {
      expected: zodDefaultLocale.types.date,
      received: zodDefaultLocale.types.string,
    }),
  );
  expect(getErrorMessage(schema.min(testDate).safeParse(new Date('2022-07-29')))).toEqual(
    interpolate(zodDefaultLocale, 'errors.too_small.date.inclusive', {
      minimum: testDate.toLocaleDateString(),
    }),
  );
  expect(getErrorMessage(schema.max(testDate).safeParse(new Date('2022-08-02')))).toEqual(
    interpolate(zodDefaultLocale, 'errors.too_big.date.inclusive', {
      maximum: testDate.toLocaleDateString(),
    }),
  );
  try {
    await schema.parseAsync(new Date('invalid'));
  } catch (err) {
    expect((err as z.ZodError).issues[0].message).toEqual(zodDefaultLocale.errors.invalid_date);
  }
});

test('array parser error messages', () => {
  const schema = z.string().array();

  expect(getErrorMessage(schema.safeParse(''))).toEqual(
    interpolate(zodDefaultLocale, 'errors.invalid_type', {
      expected: zodDefaultLocale.types.array,
      received: zodDefaultLocale.types.string,
    }),
  );
  expect(getErrorMessage(schema.min(5).safeParse(['']))).toEqual(
    interpolate(zodDefaultLocale, 'errors.too_small.array.inclusive', {
      minimum: 5,
    }),
  );
  expect(getErrorMessage(schema.max(2).safeParse(['', '', '']))).toEqual(
    interpolate(zodDefaultLocale, 'errors.too_big.array.inclusive', {
      maximum: 2,
    }),
  );
  expect(getErrorMessage(schema.nonempty().safeParse([]))).toEqual(
    interpolate(zodDefaultLocale, 'errors.too_small.array.inclusive', {
      minimum: 1,
    }),
  );
  expect(getErrorMessage(schema.length(2).safeParse([]))).toEqual(
    interpolate(zodDefaultLocale, 'errors.too_small.array.exact', {
      minimum: 2,
    }),
  );
});

test('function parser error messages', () => {
  const functionParse = z.function(z.tuple([z.string()]), z.number()).parse((a: any) => a);

  expect(getErrorMessageFromZodError(() => functionParse(''))).toEqual(
    zodDefaultLocale.errors.invalid_return_type,
  );
  expect(getErrorMessageFromZodError(() => functionParse(1 as any))).toEqual(
    zodDefaultLocale.errors.invalid_arguments,
  );
});

test('other parser error messages', () => {
  expect(
    getErrorMessage(
      z
        .intersection(
          z.number(),
          z.number().transform(x => x + 1),
        )
        .safeParse(1234),
    ),
  ).toEqual(zodDefaultLocale.errors.invalid_intersection_types);
  expect(getErrorMessage(z.literal(12).safeParse(''))).toEqual(
    interpolate(zodDefaultLocale, 'errors.invalid_literal', {
      expected: 12,
    }),
  );
  expect(getErrorMessage(z.enum(['A', 'B', 'C']).safeParse('D'))).toEqual(
    interpolate(zodDefaultLocale, 'errors.invalid_enum_value', {
      options: "'A' | 'B' | 'C'",
      received: 'D',
    }),
  );
  expect(
    getErrorMessage(
      z.object({ dog: z.string() }).strict().safeParse({ dog: '', cat: '', rat: '' }),
    ),
  ).toEqual(
    interpolate(zodDefaultLocale, 'errors.unrecognized_keys', {
      keys: "'cat', 'rat'",
    }),
  );
  expect(
    getErrorMessage(
      z
        .discriminatedUnion('type', [
          z.object({ type: z.literal('a'), a: z.string() }),
          z.object({ type: z.literal('b'), b: z.string() }),
        ])
        .safeParse({ type: 'c', c: 'abc' }),
    ),
  ).toEqual(
    interpolate(zodDefaultLocale, 'errors.invalid_union_discriminator', {
      options: "'a' | 'b'",
    }),
  );
  expect(getErrorMessage(z.union([z.string(), z.number()]).safeParse([true]))).toEqual(
    zodDefaultLocale.errors.invalid_union,
  );
  expect(
    getErrorMessage(
      z
        .string()
        .refine(() => {
          return false;
        })
        .safeParse(''),
    ),
  ).toEqual(zodDefaultLocale.errors.custom);
});

const getErrorMessage = (parsed: SafeParseReturnType<unknown, unknown>): string => {
  if ('error' in parsed) return parsed.error.issues[0].message;
  throw new Error();
};

const getErrorMessageFromZodError = (callback: () => void) => {
  try {
    callback();
  } catch (e) {
    if (e instanceof ZodError) {
      return e.errors[0].message;
    }
    throw e;
  }
};
