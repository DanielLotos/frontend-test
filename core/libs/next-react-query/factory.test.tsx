import { useInfiniteQuery, useQuery } from '@tanstack/react-query';
import { renderHook } from '@testing-library/react-hooks';

import {
  createUseEnhancedMutationHook,
  createUseInfiniteQueryHook,
  createUseQueryHook,
} from './factory';
import { EnhancedUseMutationOptions, useEnhancedMutation } from './hooks';
import { GetQueryKey } from './types';

jest.mock('@tanstack/react-query', () => ({
  useQuery: jest.fn(),
  useInfiniteQuery: jest.fn(),
  useMutation: jest.fn(),
}));

jest.mock('../api', () => {
  return {
    apiClient: {
      get: jest.fn(),
    },
    createApiClient: () => ({
      get: jest.fn(),
    }),
  };
});

jest.mock('./hooks', () => {
  return {
    useEnhancedMutation: jest.fn(),
  };
});

const mockFetcher = jest.fn(() => Promise.resolve({ anyField: true }));

describe('createUseQueryHook', () => {
  beforeEach(() => {
    (useQuery as jest.Mock).mockReset();
  });

  test('creates hook that accepts key params and useQuery options', () => {
    const getUsersQueryKey: GetQueryKey<{ userId: string }> = params => ['/users/[userId]', params];

    const defaults = { initialData: [] };
    const useUsers = createUseQueryHook<{ userId: string }, any>(getUsersQueryKey, defaults);

    const options = { cacheTime: 300, queryFn: mockFetcher };
    const keyParams = { userId: 'id' };
    renderHook(() => useUsers(keyParams, options));
    expect(useQuery).toBeCalledWith(getUsersQueryKey(keyParams), { ...defaults, ...options });
  });

  test('creates hook that accepts key params without options', () => {
    const getUsersQueryKey: GetQueryKey<{ userId: string }> = params => ['/users/[userId]', params];

    const defaults = { initialData: [] };
    const useUsers = createUseQueryHook<{ userId: string }>(getUsersQueryKey, defaults);

    const keyParams = { userId: 'id' };

    renderHook(() => useUsers(keyParams));
    expect(useQuery).toBeCalledWith(getUsersQueryKey(keyParams), { ...defaults });
  });

  test('exposes key', () => {
    const getUsersQueryKey: GetQueryKey<{ userId: string }> = params => ['/users/[userId]', params];
    const useUsers = createUseQueryHook<{ userId: string }>(getUsersQueryKey);

    const keyParams = { userId: 'id' };

    expect(useUsers.getKey(keyParams)).toEqual(getUsersQueryKey(keyParams));
  });
});

describe('createUseInfiniteQueryHook', () => {
  test('creates hook that accepts key params and useQuery options', () => {
    const getUsersQueryKey: GetQueryKey<{ order: string }> = params => ['/users', params];

    const useUsers = createUseInfiniteQueryHook<{ order: string }, any>(getUsersQueryKey);

    const options = { cacheTime: 300, queryFn: mockFetcher };
    const keyParams = { order: 'asc' };
    renderHook(() => useUsers(keyParams, options));
    expect(useInfiniteQuery).toBeCalledWith(getUsersQueryKey(keyParams), options);
  });

  test('creates hook that accepts nullable key params and BackendEntity useQuery options', () => {
    const useUsers = createUseInfiniteQueryHook<null, any>(() => ['/users', ['user']]);

    renderHook(() => useUsers(null));
    expect(useInfiniteQuery).toBeCalledWith(['/users', ['user']], {});
  });

  test('exposes key', () => {
    const getUsersQueryKey: GetQueryKey<{ userId: string }> = params => ['/users/[userId]', params];
    const useUsers = createUseInfiniteQueryHook<{ userId: string }>(getUsersQueryKey);

    const keyParams = { userId: 'id' };

    expect(useUsers.getKey(keyParams)).toEqual(getUsersQueryKey(keyParams));
  });
});

describe('createUseEnhancedMutationHook', () => {
  test('creates hook that wraps useEnhancedMutation', () => {
    const mutationFn = jest.fn().mockResolvedValue({});

    const useCreateUser = createUseEnhancedMutationHook(mutationFn);

    // eslint-disable-next-line no-console
    const options: EnhancedUseMutationOptions = { onSuccess: () => console.log('success') };
    renderHook(() => useCreateUser(options));
    expect(useEnhancedMutation).toBeCalledWith(mutationFn, options);
  });

  test('creates hook has default options', () => {
    const mutationFn = jest.fn().mockResolvedValue({});

    // eslint-disable-next-line no-console
    const defaultOptions: EnhancedUseMutationOptions = { onSuccess: () => console.log('success') };
    const useCreateUser = createUseEnhancedMutationHook(mutationFn, defaultOptions);

    renderHook(() => useCreateUser());
    expect(useEnhancedMutation).toBeCalledWith(mutationFn, defaultOptions);
  });

  test('creates hook has default options', () => {
    const mutationFn = jest.fn().mockResolvedValue({});

    // eslint-disable-next-line no-console
    const defaultOptions: EnhancedUseMutationOptions = { onSuccess: () => console.log('success') };
    const useCreateUser = createUseEnhancedMutationHook(mutationFn, defaultOptions);

    // eslint-disable-next-line no-console
    const options: EnhancedUseMutationOptions = { onError: () => console.log('error') };
    renderHook(() => useCreateUser(options));
    expect(useEnhancedMutation).toBeCalledWith(mutationFn, { ...defaultOptions, ...options });
  });
});
