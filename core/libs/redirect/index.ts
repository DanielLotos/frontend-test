import Router from 'next/router';
import { ServerResponse } from 'http';

export const redirect = async (target: string, ctx?: { res?: ServerResponse }) => {
  if (process.env.NODE_ENV === 'development') {
    // eslint-disable-next-line no-console
    console.log('%c%s', 'color: red; background: #D1D3D4; padding: 3px;', 'redirect: ', target);
  }

  if (typeof window === 'object') {
    redirect.clientRedirectPending = true;

    await Router.replace(target);

    redirect.clientRedirectPending = false;
  }

  if (ctx?.res) {
    ctx.res.writeHead(303, { Location: target });
    ctx.res.end();
  }
};

redirect.clientRedirectPending = false;
