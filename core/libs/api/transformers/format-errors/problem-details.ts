/**
 * Problem Details formatter (mostly used in .NET projects)
 */

import { allPass, assocPath, has } from 'ramda';

type ValidationErrorsExtension = {
  errors?: Record<string, string[]>;
};

type RawAPIError = {
  status: number;
  title: string;
  detail?: string;
  type: string;
  instance?: string;
} & ValidationErrorsExtension &
  Record<string, unknown>;

export const formatErrors = (data: RawAPIError) => {
  let error = null;
  let errors;
  if (!data || !isProblemDetailsFormat(data)) return {};

  //parsing errors from ValidationErrorsExtension
  //add other extensions and their parse functions if needed
  data.errors && (errors = parseValidationErrorExtension(data.errors));

  error = data.detail || data.title;
  try {
    error = JSON.parse(error);
    if (Array.isArray(error)) error = error.join(', ');
  } catch (e) {}

  return { error, errors };
};

const isProblemDetailsFormat = (data: RawAPIError): data is RawAPIError =>
  allPass([has('type'), has('title'), has('status')])(data);

const parseValidationErrorExtension = (errors: Record<string, string[]>) => {
  return Object.entries(errors).reduce(
    (acc, [key, value]) => assocPath([key], value.join(', '), acc),
    {},
  );
};
