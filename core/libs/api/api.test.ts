import { transformRequest } from './transformers';

describe('api > transformRequest', () => {
  it('return `undefined` if no body', () => {
    expect(transformRequest(undefined)).toBe(undefined);
  });

  it('stringify the body', () => {
    expect(transformRequest({ a: 42, b: '1' })).toBe(JSON.stringify({ a: 42, b: '1' }));
  });

  it('trim strings in body', () => {
    expect(transformRequest({ a: ' a  ', b: 1, c: { d: '  d  ' } })).toBe(
      JSON.stringify({ a: 'a', b: 1, c: { d: 'd' } }),
    );
  });

  it('not trim strings with whitelisted keys', () => {
    expect(
      transformRequest({
        a: ' a  ',
        b: 1,
        richText: [{ type: 'paragraph', children: [{ text: ' text ' }] }],
      }),
    ).toBe(
      JSON.stringify({
        a: 'a',
        b: 1,
        richText: [{ type: 'paragraph', children: [{ text: ' text ' }] }],
      }),
    );
  });
});
