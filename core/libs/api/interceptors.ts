import { AxiosError, AxiosInstance, AxiosResponse } from 'axios';

/* eslint-disable @typescript-eslint/naming-convention */
const errorNotifications = {
  byResponseStatus: {
    403: 'Sorry, but you don’t have permission to access this area. Try another account or connect to the support team.',
    404: 'Sorry, we couldn’t find that page. Please try to use searching or connect to the support team.',
    413: 'Oops, something went wrong, please try again or connect to the support team.',
    422: 'Violation of business rules/data integrity took place.',
    429: 'Too many requests. Try again later.',
    fallback: 'Oops, something went wrong, please try again or connect to the support team.',
  },
  offline: 'It looks like you offline',
};

const errorByStatus: { [key: string]: string } = errorNotifications.byResponseStatus;

const getFallbackMsg = (status: any, errors?: Record<string, string>) => {
  if (typeof navigator === 'object' && !navigator.onLine) {
    return errorNotifications.offline;
  }

  // show first error from errors (if exists) instead of default messages
  if (errors && Object.keys(errors).length > 0) {
    return Object.values(errors)[0];
  }

  return errorByStatus[status] || errorByStatus.fallback;
};

const getRequestTimeFromConfig = (config?: any) => {
  const requestStartedAt = config?.meta?.requestStartedAt;
  const took = requestStartedAt ? Date.now() - requestStartedAt : 'unknown';

  return `[${took}ms]`;
};

export const handleReq = (config: any) => {
  if (process.env.NODE_ENV === 'development') {
    config.meta = { ...config.meta, requestStartedAt: Date.now() };
  }

  return config;
};

export const handleRes = (res: AxiosResponse) => {
  if (process.env.NODE_ENV === 'development') {
    const method = (res.config.method || 'get').toUpperCase();
    const styles: any = {
      GET: 'color: #61affe',
      POST: 'color: #49cc90',
      PUT: 'color: #fca130',
      DELETE: 'color: #f93e3e',
    };

    if (typeof window === 'object') {
      // eslint-disable-next-line no-console
      console.log(
        `%c${method}`,
        styles[method],
        getRequestTimeFromConfig(res.config),
        res.config.url,
        res.data,
      );
    } else {
      // eslint-disable-next-line no-console
      console.log(method, getRequestTimeFromConfig(res.config), res.config.url, res.data);
    }
  }

  return res;
};

export const handleError = async (e: AxiosError<APIError>, apiClient?: AxiosInstance) => {
  const status = e?.response?.status;
  const errors = e?.response?.data?.errors ?? {};
  const error = e?.response?.data?.error ?? getFallbackMsg(status);

  const errorRes: APIError = {
    error,
    errors,
    payload: e.response?.data?.payload,
    statusCode: e.response?.status || 500,
  };

  if (process.env.NODE_ENV === 'development') {
    const method = (e.config?.method || 'get').toUpperCase();
    const styles: any = {
      GET: 'color: #61affe',
      POST: 'color: #49cc90',
      PUT: 'color: #fca130',
      DELETE: 'color: #f93e3e',
    };

    if (typeof window === 'object') {
      // eslint-disable-next-line no-console
      console.log(
        '%cERROR:',
        styles.DELETE,
        method,
        getRequestTimeFromConfig(e.response?.config),
        e.config?.url,
        errorRes,
      );
    } else {
      // eslint-disable-next-line no-console
      console.log(method, getRequestTimeFromConfig(e.response?.config), e.config?.url, errorRes);
    }
  }

  if (status === 401) {
    try {
      await apiClient?.patch('/refresh_token');
      await apiClient?.request(e.config as any);

      return;
    } catch (error) {}
  }

  return Promise.reject(errorRes);
};
