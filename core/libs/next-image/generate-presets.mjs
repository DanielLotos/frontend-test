/* eslint-disable no-console */
import { images } from '../../../frontend/next-images.config.mjs';

const format = size => `${size}=rs:fill:${size}/g:sm`;

const deviceSizes = images?.deviceSizes?.map(format);
const imageSizes = images?.imageSizes?.map(format);

console.log('\n\n# Front-end');
console.log('# deviceSizes:');
console.log(deviceSizes?.join('\n'));
console.log('# imageSizes:');
console.log(imageSizes?.join('\n'));
console.log('# /Front-end\n\n');
