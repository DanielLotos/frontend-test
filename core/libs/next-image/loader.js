// @ts-check
'use client';

/**
 * @type {import('next/image').ImageLoader}
 **/
export default function loader({ src, width, quality }) {
  const APP_URL = String(process.env.NEXT_PUBLIC_APP_URL);
  const BUCKET_URL = String(process.env.NEXT_PUBLIC_IMGPROXY_BUCKET_URL);
  const PROXY_URL = String(process.env.NEXT_PUBLIC_IMGPROXY_URL);

  const isLocalhost = /^(http|https):\/\/localhost/i.test(APP_URL);
  const isLocalImage = src.startsWith('/');
  const isImageFromBucket = !/^(http|https|\/)/.test(src);
  const normalizedSrc = src.startsWith('/') ? `${APP_URL}${src}` : src;

  // use fallback loader if proxy not set
  if (!PROXY_URL) {
    return fallbackLoader({ src, width, quality });
  }

  // use fallback loader for relative localhost image
  if (isLocalhost && isLocalImage && !isImageFromBucket) {
    return fallbackLoader({ src, width, quality });
  }

  // handle image from s3 like bucket by key with proxy
  if (!!BUCKET_URL && isImageFromBucket) {
    return `${PROXY_URL}/unsafe/${width}/plain/${BUCKET_URL}/${normalizedSrc}`;
  }

  // handle regular image with proxy
  return `${PROXY_URL}/unsafe/${width}/plain/${normalizedSrc}`;
}

/**
 * fallback loader
 * @type {import('next/image').ImageLoader}
 **/
const fallbackLoader = ({ src, width, quality }) => {
  return `${src}?w=${width}&q=${quality}`;
};
