import loader from './loader';

describe('next/image custom loader', () => {
  const originalEnv = process.env;

  beforeEach(() => {
    process.env.NEXT_PUBLIC_APP_URL = 'http://localhost:3001';
    process.env.NEXT_PUBLIC_IMGPROXY_URL = 'https://portal-img-dev.shakuro.info';
    process.env.NEXT_PUBLIC_IMGPROXY_BUCKET_URL = 's3://portal-media';
  });

  afterEach(() => {
    process.env = originalEnv;
  });

  it('image fallback loader if proxy not set (empty `NEXT_PUBLIC_IMGPROXY_URL`)', () => {
    process.env.NEXT_PUBLIC_IMGPROXY_URL = '';
    expect(loader({ src: '/t.png', width: 24, quality: 50 })).toBe('/t.png?w=24&q=50');
    expect(loader({ src: 'https://example.com/t.png', width: 24, quality: 50 })).toBe(
      'https://example.com/t.png?w=24&q=50',
    );
  });

  it('local image', () => {
    // for localhost
    expect(
      loader({
        src: '/t.png',
        width: 24,
        quality: 50,
      }),
    ).toBe('/t.png?w=24&q=50');

    // for regular usage
    process.env.NEXT_PUBLIC_APP_URL = 'https://portal-dev.shakuro.info';
    expect(
      loader({
        src: '/t.png',
        width: 24,
        quality: 50,
      }),
    ).toBe(
      'https://portal-img-dev.shakuro.info/unsafe/24/plain/https://portal-dev.shakuro.info/t.png',
    );
  });

  it('remote image', () => {
    expect(
      loader({
        src: 'https://example.com/t.png',
        width: 24,
        quality: 50,
      }),
    ).toBe('https://portal-img-dev.shakuro.info/unsafe/24/plain/https://example.com/t.png');
  });

  it('image from bucket', () => {
    expect(
      loader({
        src: 'employee_photos/5d9efcf0-5d0c-46c6-93c5-940b18771f1f.jpg',
        width: 24,
        quality: 50,
      }),
    ).toBe(
      'https://portal-img-dev.shakuro.info/unsafe/24/plain/s3://portal-media/employee_photos/5d9efcf0-5d0c-46c6-93c5-940b18771f1f.jpg',
    );
  });
});
